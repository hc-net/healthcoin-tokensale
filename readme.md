First run local
===============

    php artisan migrate
    php artisan db:seed
    php artisan coinpayments:syncexchangerates
    
Change currencies for accept payments:

    update currencies set is_accepted = 1 where code in ('BTC', 'LTCT');

Then generate pool of wallets:
    
    php artisan wallets:pool
    
Test transaction
----------------

Fill stages table:

    INSERT INTO stages (id, number, name, bonus, min, max, begin_at, expire_at, created_at, updated_at) VALUES (1, 1, 'Stage', 30.0000, 0.00000000000000000000, null, '2018-07-22 20:50:03', '2018-11-23 20:50:11', null, null);
    
Imitate accept payment:

    php artisan coins:add user_email 1 BTC

Convert all coins to tokens:

    php artisan tokens:buy


Run proxy on server
===================


