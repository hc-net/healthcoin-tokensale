let $ = require('jquery');

let $currencies = $('.select-currency-js'),
    $walletInfo = $('.wallet-info-js'),
    $walletGet = $('.wallet-get-js'),
    $walletImg = $('.wallet__img img'),
    $hiddenCurrency = $('.hidden-currency-js'),
    $walletForm = $('.form-wallet-save-js'),
    $notifyCheckEmail = $('.check-email-notify-js'),
    $crowdsaleAddress = $('.eth-crowdsale-js'),
    $reciveWallet = $('.recive-wallet-js');

const PROCESS_SAVING = "<i class='fa fa-spinner fa-spin '></i> Processing...";

$currencies.on('change', (e) => {
    e.preventDefault();
    let $selectedCurrency = $('.select-currency-js option:selected');
    let currencyId = $selectedCurrency.val();
    let currencyCode = $selectedCurrency.attr('data-currency');

    if (currencyCode === 'ETH') {

        $crowdsaleAddress.show()
        $walletInfo.hide()
    } else {

        $crowdsaleAddress.hide();
        $hiddenCurrency.val(currencyCode);
        getWallet(currencyId);
    }

});

function getWallet(currencyId) {

    let wallet = window.availableWallets[currencyId];

    if (typeof wallet === 'undefined') {
        $walletInfo.hide();
        $walletGet.show();
    } else {
        $reciveWallet.html(wallet.wallet_address)
        $walletImg.attr('src', wallet.qr)
        $walletInfo.show();
        $walletGet.hide();
    }
}


function initWallet() {
    let $selectedCurrency = $('.select-currency-js option:selected');
    let currencyCode = $selectedCurrency.attr('data-currency');
    let currencyId = $selectedCurrency.val();

    if (currencyCode === 'ETH') {
        $crowdsaleAddress.show()
    } else {
        getWallet(currencyId);
    }
}

(() => {
    initWallet()
})();

$walletForm.on('submit', function (e) {
    e.preventDefault();
    let button = $(this).find('button.wallet__key')[0];
    $(button).html(PROCESS_SAVING)
    let $form = $(this),
        $error = $('.invalid-feedback', $form),
        wallet = $(e.target).find('#wallet').val();
    if (!wallet) {
        return;
    }

    $error.html('').hide();
    $.post($form.attr('action'), $form.serializeArray(), function (data) {
    }).fail(function (data) {
        $(button).html('Save')
        if (data.responseJSON.errors) {
            $.each(data.responseJSON.errors, function (key, value) {
                $form.find('[name="' + key + '"]').parent().find('label').css({'color': 'red'})
            })
        }
        if (data.responseJSON.errors && data.responseJSON.errors.wallet) {
            $error.html(data.responseJSON.errors.wallet[0]).show();
        } else if (data.responseJSON.message) {
            $error.html(data.responseJSON.message).show();
        }
    })
    .done(() => {
        $(button).html('Save')
        $notifyCheckEmail.show()
    });

})




module.exports = {
};
