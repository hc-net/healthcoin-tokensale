$(() => {
    let $buttonAddVolume = $('.add-volume-js'),
        $inputVolume = $('.input-volume-js'),
        $formVolume = $('.volume-form'),
        $saveVolume = $('.save-volume-js'),
        resultData = {
            volumes: [],
            ids: []
        },
        current_eth = null;


    // Add Input
    $buttonAddVolume.on('click', () => {
        if (checkInput()) {
            let formVolume = $($inputVolume[0]).clone();
            $formVolume.append(formVolume);

            setId(formVolume);
            clearInputs(formVolume)
        }
    });


    // Delete Input
    document.addEventListener('click', (e) => {
        if (e.target && e.target.id === 'del-volume-js') {
            let $currentButton = $(e.target);
            let $currentInput = $currentButton.parent();
            $currentInput.remove();
            current_eth = getInputVal($formVolume.children().last(), 'eth');
            console.log(`current: ${current_eth}`)
        }
    });


    // Save form
    $saveVolume.on('click', () => {

        if ($formVolume.children().length !== 0) {
            let prev_eth = getInputVal($formVolume.children().last().prev(), 'eth');
            let last_eth = getInputVal($formVolume.children().last(), 'eth');
            let last_discount = getInputVal($formVolume.children().last(), 'discount');

            if (prev_eth >= last_eth) {
                alert('ETH could not be less or equal previous');
                return false;
            }


            if (last_eth.length === 0 || last_discount.length === 0) {
                return false;
            }
        }




        $formVolume.children().each((index, elem) => {
            let id = $(elem).data('id');
            let eth = getInputVal($(elem), 'eth');
            let discount = getInputVal($(elem), 'discount');

            let inputResult = {
                id: id,
                eth: eth,
                discount: discount
            };

            resultData.volumes.push(inputResult);
            resultData.ids.push(id);

        });

        axios.post('/admin/volume', {
            data: resultData
        })
            .then(function (response) {
                console.log(response);

                resultData = {
                    volumes: [],
                    ids: []
                };

                window.location.reload()
            })
            .catch(function (error) {
                console.log('Try later please')
            });

    });


    // Helpers

    function setId(elem) {
        let prevId = elem.prev().data('id');
        elem.attr('data-id', prevId + 1)
    }

    function getInputVal(elem, name) {
        return $(elem).find('input[name="' + name + '"]').val();
    }

    function clearInputs(elem) {
        elem.find('input').val('');
    }

    function checkInput() {
        if ($formVolume.children().length === 0) {
            return true;
        }

        let last_eth = getInputVal($formVolume.children().last(), 'eth');
        let last_discount = getInputVal($formVolume.children().last(), 'discount');

        if (last_eth.length === 0 || last_discount.length === 0) {
            return false;
        }

        if (current_eth === null) {
            current_eth = last_eth;
        } else if (last_eth > current_eth) {
            current_eth = last_eth;
        } else {
            alert('ETH could not be less or equal previous');
            return false;
        }


        return true;

    }

    (() => {
        let last_eth = getInputVal($formVolume.children().last(), 'eth');
        if (last_eth.length === 0) {
            current_eth = null;
        }

    })()

});

