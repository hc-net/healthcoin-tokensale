let $ = require('jquery');
$(() => {
    function prepareInput ($input, float) {
        if (typeof float === 'undefined') {
            float = true
        }
        $input.keypress(function (e) {
            // Allow: backspace, delete, tab, escape, enter and . and ,
            if ($.inArray(e.which, [14, 17, 8, 9, 27, 13].concat(float ? [44, 46] : [])) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.which === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                //Allow ctrl+v, ctrl+c
                (e.which === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                (e.which === 86 && (e.ctrlKey === true || e.metaKey === true))) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.which < 48 || e.which > 57))) {
                e.preventDefault();
            }
        });

        $input.keydown((e) => {
            setTimeout(() => {
                let decimals = Number($(e.target).data('decimals')) || 18,
                    pattern = new RegExp(`^[0-9]{0,100}[\\.\\,][0-9]{0,${decimals}}$`),
                    value = $input.val(),
                    dotPosition = value.search(/[,.]/);
                if (!pattern.test(value) && dotPosition !== -1) {
                    $input.val(value.substr(0, dotPosition + decimals + 1));
                }
            }, 1);
        });

        if (float) {
            $input.on('paste', function () {
                var element = this;
                setTimeout(function () {
                    var text = $(element).val();
                    $(element).val(text.replace(/[^0-9.,]/g, ""));
                }, 100);
            });
        } else {
            $input.on('paste', function () {
                var element = this;
                setTimeout(function () {
                    var text = $(element).val();
                    $(element).val(text.replace(/[^0-9]/g, ""));
                }, 100);
            });
        }

        $input.focus((e) => {
            let $this = $(e.target);
            $this.select();

            // Work around Chrome's little problem
            $this.mouseup(function() {
                // Prevent further mouseup intervention
                $this.unbind("mouseup");
                return false;
            });
        });
    }

})
