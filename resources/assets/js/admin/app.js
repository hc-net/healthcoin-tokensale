$ = require('jquery');
axios = require('axios');
require('./form-volume');

let $acceptWalletCheckbox = $('.accept-wallet-js');
let $acceptStageCheckbox = $('.active-stage-js');

$acceptWalletCheckbox.change((e) => {
    e.preventDefault();

    let sure = confirm('Are you sure?');
    let checked = $(e.target).is(':checked');

    if (sure) {
        axios.post('/admin/accept_currency', {
            checked: checked,
            id: $(e.target).attr('id')
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                alert('Try later please')
            });

    } else {

        if (checked) {
            $(e.target).prop('checked', false);
        } else {
            $(e.target).prop('checked', true);
        }
       return;
    }
});

$acceptStageCheckbox.change((e) => {
    e.preventDefault();

    let sure = confirm('Are you sure?');
    let checked = $(e.target).is(':checked');

    if (sure) {
        axios.post('/admin/active-stage', {
            checked: checked,
            id: $(e.target).attr('id')
        })
            .then(function (response) {
                window.location.reload();
            })
            .catch(function (error) {
                alert('Try later please')
            });

    } else {

        if (checked) {
            $(e.target).prop('checked', false);
        } else {
            $(e.target).prop('checked', true);
        }

    }

});