let $ = require('jquery');

$(function(){

    const copyToClipboard = str => {
        const el = document.createElement('textarea');
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };

    $('.js-copy-to-clipboard').click(function() {
        let val = $(this).closest('.tsCard').find('input').val();
        if (val) {
            copyToClipboard(val);
        }
    });
});
