let $ = require('jquery');

$(function(){

  let $calcs = $('.j-calc');
  let currencyToBottom = 'ETH';

  let statePrice = {
    send : {
      currency: {
        type: null,
        value: null
      }
    },

    get : {
      currency: {
        type: null,
        value: null
      }
    }
  };

  function setStatePrice(type, currency, value) {
    statePrice[type]['currency']['type'] = currency;
    statePrice[type]['currency']['value'] = value;
  }

  function prepareInput ($input, float) {

    if (typeof float === 'undefined') {
      float = true
    }
    $input.keypress(function (e) {
      // Allow: backspace, delete, tab, escape, enter and . and ,
      if ($.inArray(e.which, [14, 17, 8, 9, 27, 13].concat(float ? [44, 46] : [])) !== -1 ||
          // Allow: Ctrl+A, Command+A
          (e.which === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
          //Allow ctrl+v, ctrl+c
          (e.which === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
          (e.which === 86 && (e.ctrlKey === true || e.metaKey === true))) {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.which < 48 || e.which > 57))) {
        e.preventDefault();
      }
    });

    $input.keydown((e) => {
      setTimeout(() => {
        let decimals = Number($(e.target).data('decimals')) || 18,
            pattern = new RegExp(`^[0-9]{0,100}[\\.\\,][0-9]{0,${decimals}}$`),
            value = $input.val(),
            dotPosition = value.search(/[,.]/);
        if (!pattern.test(value) && dotPosition !== -1) {
          $input.val(value.substr(0, dotPosition + decimals + 1));
        }
      }, 1);
    });

    if (float) {
      $input.on('paste', function () {
        var element = this;
        setTimeout(function () {
          var text = $(element).val();
          $(element).val(text.replace(/[^0-9.,]/g, ""));
        }, 100);
      });
    } else {
      $input.on('paste', function () {
        var element = this;
        setTimeout(function () {
          var text = $(element).val();
          $(element).val(text.replace(/[^0-9]/g, ""));
        }, 100);
      });
    }

    $input.focus((e) => {
      let $this = $(e.target);
      $this.select();

      // Work around Chrome's little problem
      $this.mouseup(function() {
        // Prevent further mouseup intervention
        $this.unbind("mouseup");
        return false;
      });
    });
  }

  function calcGetVal(sendVal, sendCurrency, getCurrency, $getValInput, $priceDiscount) {

    console.log(sendCurrency);

    let ethAmount = sendVal * window.prices[sendCurrency.toUpperCase()]['ETH'];
    // console.log('ethAmount', ethAmount);
    let price = window.prices[getCurrency.toUpperCase()][sendCurrency.toUpperCase()];
    let volumeBonus = 0;
    for (let i in window.volumeBonuses) {
      let ethReq = window.volumeBonuses[i][0];
      let getBonus = window.volumeBonuses[i][1];
      if (ethAmount >= ethReq) {
        volumeBonus = getBonus;
      }
    }

    let bonus = window.stageBonus + volumeBonus;
    // console.log('bonus', bonus);
    let priceWithDiscount = price / (1 + bonus / 100);
    setStatePrice('get', getCurrency, sendVal / priceWithDiscount);
    let decimals = 10000;
    $priceDiscount.html(Math.round(priceWithDiscount * 100000000) / 100000000);
    $getValInput.val(Math.round(statePrice['get']['currency']['value'] * decimals) / decimals);


    $('.j-calc-need-to-pay-value').html(ethAmount * window.prices['ETH'][currencyToBottom] + ' ' + currencyToBottom);
  }

  function calcSendVal(getVal, sendCurrency, getCurrency, $sendValInput) {
    setStatePrice('send', sendCurrency, getVal * window.prices[sendCurrency.toUpperCase()][getCurrency.toUpperCase()])
    $sendValInput.val(statePrice['send']['currency']['value'].toFixed(4));
  }


  $calcs.each(function( index ) {
    let $calc = $(this);
    let $sendValInput = $calc.find("[name='send-value']");
    let $getValInput = $calc.find("[name='get-value']");
    let $priceDiscount = $calc.find(".j-price-discount");
    let currency = $calc.data('currency');
    if ($getValInput.length < 1 || $sendValInput.length < 1) return;

    prepareInput($sendValInput);

    $sendValInput.on('click focus blur keyup change', function() {
      let sendVal = $(this).val();
      setStatePrice('send', currency, sendVal);
      calcGetVal(sendVal, currency, 'HT', $getValInput, $priceDiscount);
    });
  });

  $(document).ready(function() {
    $("[name='send-value']").change();
  });



  window.changeCurrency = (currency) => {
      currencyToBottom = currency;
      let $calc = $('.j-calc');
      let $sendValInput = $calc.find("[name='send-value']");
      let $getValInput = $calc.find("[name='get-value']");
      let $priceDiscount = $calc.find(".j-price-discount");
      let sendVal = $sendValInput.val();
      // setStatePrice('send', currency, sendVal);
      calcGetVal(sendVal, 'ETH', 'HT', $getValInput, $priceDiscount);
    };

});
