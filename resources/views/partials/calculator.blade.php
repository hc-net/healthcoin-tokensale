<div class="tsCard">
    <div class="tsCard__header">Calculator</div>
    <div class="tsCard__body">
        <div class="j-calc" id="j-calc-eth" data-currency="ETH">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="send-value"><b>ETH:</b></label>
                    <input type="hidden" name="currency" value="ETH">
                    <input id="send-value" name="send-value" type="text" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label for="get-value"><b>HEALTH:</b></label>
                    <input id="get-value" name="get-value" type="text" class="form-control" disabled>
                </div>
            </div>

            <p>Price with discount: <span class="j-price-discount"></span></p>

            <p class="j-calc-need-to-pay" style="display: none">You need to pay <span class="j-calc-need-to-pay-value"></span></p>
        </div>
    </div>
</div>
