@extends('layouts.app')

@push('scripts')
window.prices = {!! json_encode($prices) !!};
window.stageBonus = {{ $stageBonus }};
window.volumeBonuses = {{ $volumeBonuses }};
@endpush
@section('content')
    <main class="page__body">
        <div class="container main-content">
            <div class="row justify-content-center">
                <div class="col-lg-9 col-md-12">
                    @if (session('success'))
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>{{session('success')}}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="tsCard">
                        <div class="tsCard__header">Phases</div>
                        <div class="tsCard__body">
                            <div class="phList">
                                @foreach($stages as $stage)
                                    <div class="phList__item">
                                        <div class="phList__tit">{{$stage->name}}</div>
                                        <div class="phList__dt">Up to</div>
                                        <div class="phList__pr">
                                            <p><span>{{floatval($stage->bonus + $maxVolumeBonus)}}</span>%<b></b></p>
                                        </div>
                                        <div class="phList__dt">bonus till {{date_format($stage->expire_at, 'd/m/Y')}}</div>
                                        @if($stage->month)
                                            <div class="phList__bottom">{{date_format(new DateTime("2000-$stage->month-01"), 'M')}}</div>
                                        @endif
                                    </div>

                                @endforeach

                            </div>
                        </div>
                    </div>

                        @include('partials.calculator')

                    <div class="tsCard" id="account">
                        <div class="tsCard__body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group wallet__select">
                                        <label for="get-value"><b>Choose a cryptocurrency</b></label>
                                        <select class="form-control select-currency-js">
                                            @foreach($acceptedCoinCurrencies as $currency)
                                                <option
                                                        value="{{$currency->id}}"
                                                        @if(session('selectedCurrency') && session('selectedCurrency') === $currency->code)
                                                            selected
                                                        @elseif(!session('selectedCurrency') && $currency->code == 'ETH')
                                                            selected
                                                        @endif
                                                        data-currency="{{$currency->code}}"
                                                >{{$currency->code}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="alert alert-warning fade show check-email-notify-js" role="alert"  @if(is_null($walletConfirmation)) style="display:none;"  @endif>
                                        <strong>Check your email to confirm the wallet</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row  eth-crowdsale-js" style="display: none">
                                <div class="col-sm-8">
                                <div class="wallet__tit">To buy Health Token send ETH from your wallet to SmartContract and you receive tokens back</div>
                                        <input
                                        type="text"
                                        value="{{env('CROWDSALE_ADDRESS')}}"
                                        class="form-control wallet__key wallet-address-js"
                                        name="wallet"
                                        disabled>
                                </div>

                                <div class="col-sm-4">
                                    <div class="wallet__img"><img src="{{$crowdSaleAddress->qrBase64}}" alt=""></div>
                                </div>
                            </div>

                            <div class="row wallet-info-js" style="display: none">
                                <div class="col-sm-8">
                                    <div class="wallet__tit">Enter your etherium wallet you receive tokens here: <strong class="recive-wallet-js"></strong></div>
                                    <form class="form-wallet-save-js" action="{{ route('save-wallet') }}">
                                        @csrf
                                        <input
                                                id="wallet"
                                                type="text"
                                                value="{{ old('wallet') ?: ($user->ether_account ? $user->ether_account->wallet : '')}}"
                                                class="form-control wallet__key wallet-address-js"
                                                name="wallet"
                                                placeholder="Enter wallet"
                                        >
                                        <span class="invalid-feedback" role="alert"></span>
                                        <button
                                                type="submit"
                                                class="wallet__key form-control btn btn-primary"
                                        >{{ __('Save') }}</button>
                                    </form>
                                </div>

                                <div class="col-sm-4">
                                    <div class="wallet__img"><img src="" alt=""></div>
                                </div>
                            </div>


                            <div class="row wallet-get-js" style="display: none">
                                <div class="col-md-12">
                                    <form action="{{route('get-bank-wallet')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="currency" class="hidden-currency-js" value="">
                                        <button class="btn btn-default">Get wallet</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12">
                    <div class="balance">
                        <div class="balance__tit">Balance</div>
                        <div class="balance__txt">1 {{env('CURRENT_TOKEN_NAME')}} = {{env('TOKEN_PRICE_ETH')}} ETH</div>
                    </div>

                    <div class="tsCard tsCard_thin">
                        <div class="tsCard__header">
                            Referral Link
                            <button class="btn btn-primary js-copy-to-clipboard">Copy</button>
                        </div>
                        <div class="tsCard__body">
                            <input class="form-control" type="text" value="{{ route('register', ['ref' => $user->referral_code]) }}" disabled="disabled" />
                        </div>
                    </div>

                    <div class="tsCard tsCard_thin">
                        <div class="tsCard__header">Token Sale Bonus</div>
                        <div class="tsCard__body">
                            <p>Step 1&nbsp;&ndash; August/September&nbsp;&ndash; 25%</p>
                            <p>Step 2&nbsp;&ndash; October&nbsp;&ndash; 15%</p>
                            <p>Step 3&nbsp;&ndash; November&nbsp;&ndash; 5%</p>
                        </div>
                    </div>

                    <div class="tsCard tsCard_thin">
                        <div class="tsCard__header">Bonuses from the volume</div>
                        <div class="tsCard__body">
                            <p>From 10 ETH&nbsp;&ndash; 5%</p>
                            <p>From 50 ETH&nbsp;&ndash; 10%</p>
                            <p>From 100 ETH&nbsp;&ndash; 15%</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        window.availableWallets = {!! json_encode($availableWallets) !!};
    </script>
@endsection

