@extends('layouts.app')

@section('content')
        <div class="face"><img src="./img/Logo_white.png" alt="#" class="face__img">
            <div class="face__txt">
                <div class="face__title">Tokensale HealthCoinNet</div>
                <div class="face__sub">The coin to reinvent health and wellness payment systems</div>
            </div>
            <a href="{{route('register')}}" class="face__btn"
               onclick="gtag('event', 'presale', { 'event_category' : 'button',}); yaCounter50037886.reachGoal ('button1'); return true;">Join
                Pre-Sale</a>
        </div>
@endsection
