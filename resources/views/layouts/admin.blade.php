<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script src="{{ asset(mix('js/admin/app.js')) }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body style="background-color: #fff;">
    <div class="container-full" style="padding: 30px;">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    <a href="{{route('admin')}}" class="list-group-item list-group-item-action @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'admin') active @endif">Dashboard</a>
                    <a href="{{route('stages')}}" class="list-group-item list-group-item-action @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'stages') active @endif">Стэйджи</a>
                    <a href="{{route('transfers')}}" class="list-group-item list-group-item-action @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'transfers') active @endif">Трансферы</a>
                    <a href="{{route('users')}}" class="list-group-item list-group-item-action @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'users') active @endif">Пользователи</a>
                    <a href="{{route('currencies')}}" class="list-group-item list-group-item-action @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'currencies') active @endif">Валюты</a>
                    <a href="{{route('volume')}}" class="list-group-item list-group-item-action @if(\Illuminate\Support\Facades\Route::currentRouteName() == 'volume') active @endif">Volume Discount</a>
                </div>
            </div>
            <div class="col-md-9">
                @yield('content')
            </div>
        </div>
    </div>
</body>
</html>
