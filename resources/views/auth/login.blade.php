@extends('layouts.app')

@section('content')
    <div class="regForm">
        @if(session('send_confirm'))
            <div class="alert alert-primary" role="alert">
                {{ session('send_confirm') }}
            </div>
            @push('scripts')
                window.onload = function() {
                    gtag('event', 'presale', { 'event_category' : 'Form', }); yaCounter50037886.reachGoal ('presale');
                };
            @endpush
        @endif

        @if(session('confirmed'))
            <div class="alert alert-success" role="alert">
                {{ session('confirmed') }}
            </div>
        @endif

        <div class="regForm__tit">{{ __('Login') }}</div>
        <form method="POST" class="regForm__form" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf

            <input id="email"
                   type="email"
                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                   name="email"
                   value="{{ old('email') }}"
                   placeholder="Email"
                   required autofocus>

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif


            <input id="password"
                   type="password"
                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                   name="password"
                   placeholder="Password"
                   required>

            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif


            <div class="form-group row">
                <div class="col-md-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
            </div>


            <button type="submit" class="regForm__btn">
                {{ __('Login') }}
            </button>


            <div class="regForm__cite"><a  href="{{ route('password.request') }}"> {{ __('Forgot Your Password?') }}</a></div>

        </form>
    </div>

@endsection
