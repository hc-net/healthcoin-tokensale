@extends('layouts.app')

@section('content')
    <div class="regForm">
        <form method="POST" class="regForm__form" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
            @csrf

            <input id="email"
                   type="email"
                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                   name="email"
                   value="{{ old('email') }}"
                   placeholder="Email"
                   required>

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif


            <button type="submit" class="regForm__btn">
                {{ __('Send Password Reset Link') }}
            </button>
        </form>
    </div>

@endsection
