@extends('layouts.app')

@section('content')
        <div class="regForm">
            <div class="regForm__tit">{{ __('Register') }}</div>
            <form method="POST" class="regForm__form" action="{{ route('register') }}"
                  aria-label="{{ __('Register') }}">
                @csrf

                @if (request()->has('ref'))
                    <input type="hidden" name="ref" value="{{ request()->get('ref') }}" />
                @endif

                <input id="name"
                       type="text"
                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                       name="name"
                       value="{{ old('name') }}"
                       placeholder="Name"
                       required autofocus>

                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif

                <input id="email"
                       type="email"
                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                       name="email"
                       value="{{ old('email') }}"
                       placeholder="Email"
                       required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif


                <input id="password" type="password"
                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                       name="password"
                       placeholder="Password"
                       required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif


                <input id="password-confirm"
                       type="password"
                       class="form-control"
                       name="password_confirmation"
                       placeholder="Confirm Password"
                       required>



                <button type="submit" class="regForm__btn">
                    {{ __('Register') }}
                </button>
                <div class="regForm__cite">Or<a href="{{ route('login') }}">{{ __('Login') }}</a></div>

            </form>
        </div>
@endsection
