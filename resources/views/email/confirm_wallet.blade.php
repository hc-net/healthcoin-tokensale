
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <h2 class="no-border-header">Hello{{ $confirmation->user->name ? ', ' . $confirmation->user->name : '' }}!</h2>
            <p class="medium-font">We received a request to change your wallet address:<br>
                IP address: {{ $confirmation->change_ip }}<br>
                {{ $confirmation->location }}<br>
                Your new wallet: {{ $confirmation->address }}
            </p>
        </td>
    </tr>
    <tr align="center">
        <td>
            <a href="{{ $confirmation->confirmUrl }}" class="big-btn">Change ERC-20 address</a>
        </td>
    </tr>
    <tr>
        <td>
            <p class="medium-font">If the button above doesn’t work, paste this link into your web browser:
                <br>
                {!! $noClickableUrl !!}
                <br>
            </p>
        </td>
    </tr>
</table>