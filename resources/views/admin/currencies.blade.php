@extends('layouts.admin')

@section('content')
    <h1>Currencies</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Code</th>
            <th scope="col">Is Accepted</th>
        </tr>
        </thead>
        <tbody>
        @foreach($currencies as $currency)
            <tr>
                <td>{{$currency->name}}</td>
                <td>{{$currency->code}}</td>
                <td><input type="checkbox" @if($currency->is_accepted) checked @endif class="accept-wallet-js" id="{{$currency->id}}"></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection