@extends('layouts.admin')

@section('content')
    <h1>Transfers</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">type</th>
            <th scope="col">user</th>
            <th scope="col">amount</th>
            <th scope="col">date</th>
            <th scope="col">reference</th>
        </tr>
        </thead>
        <tbody>
        @foreach($transfers as $transfer)
            <tr>
                <td>{{$transfer->type}}</td>
                @if($transfer->type == \App\Models\Accounts\Transfer::TYPE_COIN_SPENDING)
                    <td>
                        @if($transfer->source)
                            @if($transfer->source->user)
                                {{$transfer->source->user->email}}
                            @endif
                        @endif
                    </td>
                @else
                    <td>
                        @if($transfer->destination)
                            @if($transfer->destination->user)
                                {{$transfer->destination->user->email}}
                            @endif
                        @endif
                    </td>
                @endif
                <td>
                    @if($transfer->type == \App\Models\Accounts\Transfer::TYPE_COIN_SPENDING)
                        -
                    @endif
                    {{$transfer->amount}} {{$transfer->source->currency->code}}
                </td>
                <td>{{$transfer->created_at}}</td>
                <td>{{$transfer->reference}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
