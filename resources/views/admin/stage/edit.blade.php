@extends('layouts.admin')
@section('content')
    <h1>Edit stage</h1>

    <form class="needs-validation" novalidate action="{{ $stage->id ? route('stage-update') : route('stage-store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Название</label>
            <input type="text" class="form-control" placeholder="Название" value="{{$stage->name}}" name="name">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Bonus</label>
            <input type="number" class="form-control" placeholder="Bonus" value="{{$stage->bonus}}" name="bonus">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Min</label>
            <input type="number" class="form-control" placeholder="Min" value="{{$stage->min}}" name="min">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Max</label>
            <input type="number" class="form-control" placeholder="Max" value="{{$stage->max}}" name="max">
        </div>

        <div class="form-group">
            <label for="monthInput">Month</label>
            <input type="number" class="form-control" placeholder="Month" value="{{$stage->month}}" name="month" id="monthInput">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">expire_at</label>
            <input type="datetime-local" class="form-control" placeholder="expire_at" value="{{$stage->expire_at ? $stage->expire_at->format('Y-m-d\TH:i') : ''}}" name="expire_at">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-info">Сохранить</button>
        </div>

        <input type="hidden" name="stage_id" value="{{$stage->id}}">

    </form>
@endsection



