@extends('layouts.admin')

@section('content')
    <h1>Stages</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">name</th>
            <th scope="col">bonus</th>
            <th scope="col">min</th>
            <th scope="col">max</th>
            <th scope="col">expired_at</th>
            <th scope="col">month</th>
            <th scope="col">active</th>
            <th scope="col">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($stages as $stage)
            <tr>
                <td>{{$stage->name}}</td>
                <td>{{$stage->bonus}}</td>
                <td>{{$stage->min}}</td>
                <td>{{$stage->max}}</td>
                <td>{{$stage->expire_at}}</td>
                <td>{{$stage->month}}</td>
                <td><input type="checkbox" @if($stage->is_active) checked @endif class="active-stage-js" id="{{$stage->id}}"></td>
                <td>
                    <a href="{{route('stage-edit', ['id' => $stage->id])}}" class="btn btn-info">Изменить</a>
                    <form action="{{ route('stage-delete', ['stage' => $stage->id]) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-warning">Удалить</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('stage-create') }}" class="btn btn-success">Add new</a>
@endsection
