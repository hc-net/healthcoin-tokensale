@extends('layouts.admin')

@section('content')
    <h1>Users</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">email</th>
            <th scope="col">ETH wallet</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->email}}</td>
                <td>{{ old('wallet') ?: ($user->ether_account ? $user->ether_account->wallet : '')}}</td>
                <td><a href="{{route('users-show', ['id' => $user->id])}}" class="btn btn-info">Показать</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection