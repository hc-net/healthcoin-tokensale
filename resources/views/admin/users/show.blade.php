@extends('layouts.admin')

@section('content')
    <h1>User info: {{$user->email}}</h1>
    <hr>
    @if (! is_null($user->referrer))
        <h2>Referrer: <a href="{{ route('users-show', ['id' => $user->referrer->id]) }}">{{ $user->referrer->email }}</a></h2>
        <hr />
    @endif
    <h2>Transfers</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">type</th>
            <th scope="col">amount</th>
            <th scope="col">date</th>
            <th scope="col">reference</th>
        </tr>
        </thead>
        <tbody>
        @foreach($transfers as $transfer)
            <tr>
                <td>{{$transfer->type}}</td>
                <td>
                    {{$transfer->type == \App\Models\Accounts\Transfer::TYPE_COIN_SPENDING ? '-' : ''}}{{$transfer->amount}}
                    {{$transfer->source->currency->code}}
                </td>
                <td>{{$transfer->created_at}}</td>
                <td>{{$transfer->reference}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h2>Accounts</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">code</th>
            <th scope="col">wallet</th>
            <th scope="col">balance</th>
        </tr>
        </thead>
        <tbody>
        @foreach($accounts as $account)
            <tr>
                <td>{{$account->currency->code}}</td>
                <td>{{$account->wallet}}</td>
                <td>{{$account->balance_round}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @if ($user->referrals->count() > 0)
    <h2>Referrals</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">email</th>
            <th scope="col">name</th>
        </tr>
        </thead>
        <tbody>
        @foreach($user->referrals as $referral)
            <tr>
                <td><a href="{{route('users-show', ['id' => $referral->id])}}">{{$referral->email}}</td>
                <td>{{$referral->name}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif
@endsection
