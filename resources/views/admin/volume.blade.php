@extends('layouts.admin')

@section('content')
    <h1>Volume Discount</h1>

    <div class="col-md-6">
        <form class="volume-form" method="post">

            {{--<div class="d-inline-flex align-items-center input-volume-js" id="0">--}}
                {{--<label class="my-1 mr-2">ETH</label>--}}
                {{--<input type="text" class="form-control mb-2 mr-sm-2" name="eth">--}}
                {{--<input type="text" class="form-control mb-2 mr-sm-2" name="discount">--}}
                {{--<label class="my-1 mr-2">%</label>--}}
                {{--<button class="btn btn-danger del-volume-js" id="del-volume-js" type="button">Delete</button>--}}
            {{--</div>--}}

            @if($volumes->isEmpty())
                <div class="d-inline-flex align-items-center input-volume-js" data-id="1">
                    <label class="my-1 mr-2">ETH</label>
                    <input type="text" class="form-control mb-2 mr-sm-2" name="eth">
                    <input type="text" class="form-control mb-2 mr-sm-2" name="discount">
                    <label class="my-1 mr-2">%</label>
                    <button class="btn btn-danger del-volume-js" id="del-volume-js" type="button">Delete</button>
                </div>
            @else
                @foreach($volumes as $volume)
                    <div class="d-inline-flex align-items-center input-volume-js" data-id="{{$volume->id}}">
                        <label class="my-1 mr-2">ETH</label>
                        <input type="text" class="form-control mb-2 mr-sm-2" name="eth" value="{{$volume->eth}}">
                        <input type="text" class="form-control mb-2 mr-sm-2" name="discount" value="{{$volume->discount}}">
                        <label class="my-1 mr-2">%</label>
                        <button class="btn btn-danger del-volume-js" id="del-volume-js" type="button">Delete</button>
                    </div>
                @endforeach
            @endif
        </form>
        <hr>
      <div class="col-md-3 d-inline-flex justify-content-between">
          <button class="btn btn-default add-volume-js">Add</button>
          <button class="btn btn-default save-volume-js">Save</button>
      </div>
    </div>

@endsection

