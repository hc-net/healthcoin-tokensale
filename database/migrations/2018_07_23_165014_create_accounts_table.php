<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('type', \App\Models\Accounts\Account::TYPES);
            $table->enum('status', \App\Models\Accounts\Account::STATUSES);
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('currency_id')->unsigned();
            $table->decimal('balance', 20, 8)->default(0.0);
            $table->string('wallet')->nullable()->unique();
            $table->string('dest_tag')->nullable();
            $table->string('pubkey')->nullable();
            $table->decimal('credit_limit', 20, 8)->nullable()->default(0.0);

            $table->timestamps();

            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function(Blueprint $table) {
            $table->dropForeign('user_id');
            $table->dropForeign('account_type_id');
        });
        Schema::dropIfExists('accounts');
    }
}
