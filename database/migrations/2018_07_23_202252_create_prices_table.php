<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('commodity_id')->unsigned();
            $table->integer('currency_id')->unsigned();
            $table->dateTime('date');
            $table->decimal('value', 32, 20);

            $table->timestamps();

            $table->unique(['commodity_id', 'currency_id']);
            $table->foreign('commodity_id')->references('id')->on('currencies');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->index('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
