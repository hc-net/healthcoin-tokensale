<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('currencies', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique()->nullable();
            $table->enum('type', \App\Models\Accounts\Currency::TYPES)->default(\App\Models\Accounts\Currency::TYPE_COIN);
            $table->boolean('is_accepted')->default(false);

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('currencies');
	}

}
