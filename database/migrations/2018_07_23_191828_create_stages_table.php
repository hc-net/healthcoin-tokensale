<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stages', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('number')->nullable();
            $table->string('name')->nullable();
            $table->decimal('bonus', 8, 4)->nullable();
            $table->decimal('min', 32, 20)->nullable();
            $table->decimal('max', 32, 20)->nullable();
            $table->timestamp('begin_at')->nullable();
            $table->timestamp('expire_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stages');
    }
}
