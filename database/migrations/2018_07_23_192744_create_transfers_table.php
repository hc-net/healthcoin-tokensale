<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->increments('id');

            $table->enum('type', \App\Models\Accounts\Transfer::TYPES);
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('source_id')->unsigned();
            $table->integer('destination_id')->unsigned();
            $table->decimal('amount', 32, 20);
            $table->string('reference')->nullable();
            $table->string('description')->nullable();
            $table->decimal('price', 32, 20)->nullable();
            $table->decimal('ratio', 32, 20)->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('stage_id')->unsigned()->nullable();
            $table->string('bonuses', 2000)->nullable();

            $table->timestamps();

            $table->unique('reference');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('source_id')->references('id')->on('accounts')->onDelete('restrict');
            $table->foreign('destination_id')->references('id')->on('accounts')->onDelete('restrict');
            $table->foreign('parent_id')->references('id')->on('transfers');
            $table->foreign('stage_id')->references('id')->on('stages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
