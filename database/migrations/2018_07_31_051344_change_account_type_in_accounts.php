<?php

use Illuminate\Database\Migrations\Migration;
use App\Models\Accounts\Account;

class ChangeAccountTypeInAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table = (new Account())->getTable();
        $values = implode(', ', array_map(function ($e) {return "'$e'";},Account::TYPES));
        DB::statement("ALTER TABLE $table MODIFY COLUMN type ENUM($values)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
