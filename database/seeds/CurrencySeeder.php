<?php

use App\Models\Accounts\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::firstOrCreate([
            'name' => 'Bitcoin',
            'code' => 'BTC',
            'type' => Currency::TYPE_COIN
        ]);
        Currency::firstOrCreate([
            'name' => 'Etherium',
            'code' => 'ETH',
            'type' => Currency::TYPE_COIN
        ]);
        Currency::firstOrCreate([
            'name' => 'Litecoin Testnet',
            'code' => 'LTCT',
            'type' => Currency::TYPE_COIN
        ]);
        Currency::firstOrCreate([
            'name' => config('app.current_token_name'),
            'code' => config('app.current_token'),
            'type' => Currency::TYPE_TOKEN
        ]);
    }
}
