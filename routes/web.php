<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (\Illuminate\Support\Facades\Auth::user()) {
        return redirect('/home');
    } else {
        return view('welcome');
    }

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/save-wallet', 'HomeController@saveWallet')->name('save-wallet');
Route::post('/get-bank-wallet', 'HomeController@getBankWallet')->name('get-bank-wallet');
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');
Route::get('confirm-wallet/{token}', 'HomeController@confirmWallet')->name('confirm_wallet');

Route::middleware('admin')->group(function () {
    Route::namespace('Admin')->group(function () {

        Route::prefix('admin')->group(function () {

            Route::get('/', 'AdminController@index')->name('admin');
            Route::get('/transfers', 'TransferController@index')->name('transfers');
            Route::get('/currencies', 'AdminController@currencies')->name('currencies');
            Route::post('/accept_currency', 'AdminController@acceptCurrency');
            Route::post('/active-stage', 'AdminController@activeStage');


            Route::prefix('volume')->group(function() {
                Route::get('/', 'VolumeController@index')->name('volume');
                Route::post('/', 'VolumeController@store')->name('volume-save');
            });


            Route::prefix('stages')->group(function () {
                Route::get('/', 'StageController@index')->name('stages');
                Route::get('/edit/{id}', 'StageController@edit')->name('stage-edit');
                Route::get('/create', 'StageController@create')->name('stage-create');
                Route::post('/update', 'StageController@update')->name('stage-update');
                Route::post('/store', 'StageController@store')->name('stage-store');
                Route::post('/delete/{stage}', 'StageController@delete')->name('stage-delete');
            });

            Route::prefix('users')->group(function () {
                Route::get('/', 'UserController@index')->name('users');
                Route::get('/{id}', 'UserController@show')->name('users-show');
            });

        });


    });
});

