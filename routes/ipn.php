<?php

Route::prefix('coinpayments')->group(function () {
    Route::post('transaction', 'CoinpaymentsIPNController@transaction')->name('coinpayments.transactionIpnUrl');
    Route::post('deposit', 'CoinpaymentsIPNController@deposit')->name('coinpayments.depositIpnUrl');
    Route::post('callbackAddress', 'CoinpaymentsIPNController@callbackAddress')->name('coinpayments.callbackAddressIpnUrl');
});
