<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\Auth\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'coinpayments' => [
        'merchant_id' => env('COINPAYMENTS_MERCHANT_ID'),
        'public_key' => env('COINPAYMENTS_PUBLIC_KEY'),
        'private_key' => env('COINPAYMENTS_PRIVATE_KEY'),
        'ipn_secret' => env('COINPAYMENTS_IPN_SECRET'),
        'ipn_host' => env('COINPAYMENTS_IPN_HOST'),
    ],

    'ethereum' => [
        'scheme' => env('ETHEREUM_SCHEME', 'http'),
        'host' => env('ETHEREUM_HOST', 'localhost'),
        'port' => env('ETHEREUM_PORT', 3000),
    ],

];
