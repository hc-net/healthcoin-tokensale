<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmWallet;
use App\Models\Accounts\Account;
use App\Models\Accounts\Currency;
use App\Models\Accounts\Stage;
use App\Models\Accounts\WalletConfirmation;
use App\Models\Auth\User;
use App\Services\AccountService;
use App\Services\CurrencyService;
use App\Services\PriceService;
use App\Services\StageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    private $accountService;
    private $currencyService;
    private $priceService;
    private $stageService;

    /**
     * Create a new controller instance.
     *
     * @param CurrencyService $currencyService
     * @param AccountService $accountService
     */
    public function __construct(
        CurrencyService $currencyService,
        AccountService $accountService,
        PriceService $priceService,
        StageService $stageService
    )
    {
        $this->currencyService = $currencyService;
        $this->accountService = $accountService;
        $this->priceService = $priceService;
        $this->stageService = $stageService;
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Validation\Validator
     */
    protected function walletValidator(Request $request)
    {
        return Validator::make($request->except(['_token']), [
            'wallet' => 'required|eth_address',
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Validation\Validator
     */
    protected function bankWalletValidator(Request $request)
    {
        return Validator::make($request->except(['_token']), [
            'currency' => 'required|in:' . implode(',', $this->currencyService->getAcceptedCoinCurrencies()->pluck('code')->toArray()),
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var User $user */
        $availableWallets = [];
        $stages = Stage::all();
        $user = Auth::user();
        $walletConfirmation = !$user->walletConfirmations->isEmpty() ? $user->walletConfirmations->last()->address : null;
        $acceptedCoinCurrencies = $this->currencyService->getAcceptedCoinCurrencies();

        foreach ($acceptedCoinCurrencies as $currency) {
            $bankAccount = $user->getBankAccount($currency);

            if ($bankAccount) {

                $availableWallets[$currency->id] = [
                    'currency_id'    => $currency->id,
                    'wallet_address' => $bankAccount->wallet,
                    'qr'             => $bankAccount->qrBase64,
                    'currency_code'  => $currency->code
                ];

            }
        }

        $crowdSaleAddress = $this->accountService->getCrowdsaleAddress();
        $currencyETH = Currency::where('code', 'ETH')->first();
        $ps = [
            'ETHBTC' => $this->priceService->getPrice('ETH', 'BTC'),
            'BTCETH' => $this->priceService->getPrice('BTC', 'ETH'),
            'ETHLTCT' => $this->priceService->getPrice('ETH', 'LTCT'),
            'LTCTETH' => $this->priceService->getPrice('LTCT', 'ETH'),
            'LTCTBTC' => $this->priceService->getPrice('LTCT', 'BTC'),
            'BTCLTCT' => $this->priceService->getPrice('BTC', 'LTCT'),
            'HTETH' => env('TOKEN_PRICE_ETH'),
        ];
        $ps['ETHHT'] = bcdiv('1', $ps['HTETH'], 4);
        $prices = [
            'HT' => ['HT' => 1, 'ETH' => (float)$ps['HTETH'], 'BTC' => (float)bcmul($ps['HTETH'], $ps['ETHBTC'], 8), 'LTCT' => (float)bcmul($ps['HTETH'], $ps['ETHLTCT'], 8) ],
            'ETH' => ['HT' => (float)$ps['ETHHT'], 'ETH' => 1, 'BTC' => (float)$ps['ETHBTC'], 'LTCT' => (float)$ps['ETHLTCT'] ],
            'BTC' => ['HT' => (float)bcmul($ps['ETHHT'], $ps['BTCETH'], 8), 'ETH' => (float)$ps['BTCETH'], 'BTC' => 1, 'LTCT' => (float)$ps['BTCLTCT']],
            'LTCT' => ['HT' => (float)bcmul($ps['ETHHT'], $ps['LTCTETH'], 8), 'ETH' => (float)$ps['LTCTETH'], 'BTC' => (float)$ps['LTCTBTC'], 1]
        ];
        $stageBonus = $this->stageService->getCurrentStage()->bonus;
        $volumeBonuses = env('VOLUME_BONUS');
        $maxVolumeBonus = 0;
        foreach (json_decode($volumeBonuses) as $volumeBonus) {
            $maxVolumeBonus = $volumeBonus[1];
        }
        return view('home',
            compact(
            'user',
            'acceptedCoinCurrencies',
            'crowdSaleAddress',
            'currencyETH',
            'prices',
            'stageBonus',
            'volumeBonuses',
            'maxVolumeBonus',
            'walletConfirmation',
            'stages',
            'availableWallets'
        ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws ValidationException
     */
    public function getBankWallet(Request $request)
    {
        $validator = $this->bankWalletValidator($request);
        $data = $validator->validate();
        $currency = Currency::where('code', $data['currency'])->first();
        /** @var User $user */
        $user = Auth::user();
        $currentUserAccount = $user->getBankAccount($currency);

        if (empty($currentUserAccount)) {

            $account = Account::whereNull('user_id')
                ->where('type', Account::TYPE_BANK_PAYMENT)
                ->where('currency_id', $currency->id)
                ->first();

            if (!empty($account)) {
                $account->user_id = $user->id;
                $account->save();
            }
        }

        return redirect('/home#account')->with('selectedCurrency', $currency->code);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     * @throws \App\Exceptions\Accounts\CurrencyNotExists
     * @throws \App\Exceptions\Accounts\UndefinedTypeAccountException
     */
    public function saveWallet(Request $request)
    {

        $validator = $this->walletValidator($request);
        $validator->validate();

        $user = $request->user();
        $wallet = $request->input('wallet');

        if (Account::where('wallet', $wallet)->count() > 0) {
            $validator->errors()->add('wallet', trans('validation.wallet_exists'));
            throw new ValidationException($validator);
        }

        $token = Str::random(40);

        $confirmation = new WalletConfirmation([
            'address' => $wallet,
            'token' => $token,
            'change_ip' => get_real_ip(),
        ]);

        $confirmation->user()->associate($user);
        $confirmation->save();

        Mail::queue(new ConfirmWallet($confirmation));

        return redirect()->back();
    }


    public function confirmWallet(Request $request, $token) {
        /** @var User $user */
        $user = $request->user();
        $redirect = redirect()->route('home');
        if (!$token) {
            return $redirect->with('error', 'Invalid token');
        }
        if (!$user->walletConfirmations()->where('token', $token)->exists()) {
            return $redirect->with('error', 'Invalid token');
        }

            $walletConfirmation = $user->walletConfirmations()->where('token', $token)->first();

            if ($user->ether_account) {
                $user->etherAccounts()->update([
                    'type' => Account::TYPE_USER_ETHER_OLD,
                ]);
            }

            $params = ['wallet' => $walletConfirmation->address, 'type' => Account::TYPE_USER_ETHER];
            if ($user->accounts()->where($params)->exists()) {
                $newAccount = $user->accounts()->where($params)->first();
                $newAccount->save();
            } else {
                $newAccount = $this->accountService->create([
                    'user' => $user,
                    'currency' => 'ETH',
                    'status' => Account::STATUS_CREATED,
                    'wallet' => ['address' => $walletConfirmation->address],
                    'type' => Account::TYPE_USER_ETHER,
                ]);
            }
            $walletConfirmation->confirmed_at = now();
            $walletConfirmation->confirm_ip = get_real_ip();
            $walletConfirmation->save();



        return $redirect->with('success', 'Wallet accepted!');
    }

}
