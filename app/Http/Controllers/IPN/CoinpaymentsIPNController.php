<?php

namespace App\Http\Controllers\IPN;

use App\Exceptions\Accounts\AccountException;
use App\Exceptions\Accounts\ClosedAccountException;
use App\Exceptions\Accounts\InsufficientFundsException;
use App\Exceptions\Accounts\InvalidAmountException;
use App\Exceptions\IPN\HMACSignatureDoesNotMatchException;
use App\Exceptions\IPN\NoHMACSignatureException;
use App\Http\Controllers\Controller;
use App\Services\AccountService;
use App\Services\PriceService;
use App\Services\TransferService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CoinpaymentsIPNController extends Controller {

    protected $stageService;
    protected $transferService;
    protected $accountService;
    protected $priceService;

    public function __construct(
        TransferService $transferService,
        PriceService $priceService,
        AccountService $accountService
    ) {
        parent::__construct();
        $this->transferService = $transferService;
        $this->accountService = $accountService;
        $this->priceService = $priceService;
    }

    protected function validator(array $data) {
        return Validator::make($data, [
            'merchant' => [
                'required',
                Rule::in([config('services.coinpayments')['merchant_id']]),
            ],
            'ipn_type' => 'required|in:simple,button,cart,donation,deposit,api',
            'status' => 'required',
            'txn_id' => 'required',
        ]);
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function depositValidator(array $data) {
        return Validator::make($data, [
            'merchant' => [
                'required',
                Rule::in([config('services.coinpayments')['merchant_id']]),
            ],
            'ipn_type' => 'required|in:deposit',
            'status' => 'required',
            'txn_id' => 'required',
            'currency' => 'required',
            'address' => 'required',
            'amount' => 'required',
        ]);
    }

    /**
     * Проверяем, подписан ли запрос секретным ключом.
     * @param Request $request
     * @throws HMACSignatureDoesNotMatchException
     * @throws NoHMACSignatureException
     */
    private function checkRequest(Request $request)
    {
        if (config('app.env') === 'local') {
            return;
        }
        $hmacHeader = $request->header('Hmac');
        if (!$hmacHeader) {
            throw new NoHMACSignatureException();

        }
        $hmac = hash_hmac("sha512", $request->getContent(), config('services.coinpayments')['ipn_secret']);
        if ($hmac !== $hmacHeader) {
            throw new HMACSignatureDoesNotMatchException();
        }
    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deposit(Request $request) {
        $data = $request->all();
        $validator = $this->depositValidator($data);
        if ($validator->fails()) {
            Log::error('COINPAYMENTS FAILS 1', $data);
            return response('');
        }

        try {
            $this->checkRequest($request);
        } catch (NoHMACSignatureException $e) {
            Log::error('COINPAYMENTS FAILS 2.1', $data);
            return response('No HMAC signature sent', 400);
        } catch (HMACSignatureDoesNotMatchException $e) {
            Log::error('COINPAYMENTS FAILS 2.2', $data);
            return response('HMAC signature does not match', 400);
        }

        // Генерим уникальнаю строку для обозначения трансфера в нашей системе
        // $data['txn_id'] - айди транзакции в блокчейне
        // $data['ipn_id'] - айди транзакции coinpayments
        $reference = $data['txn_id'] . (array_key_exists('ipn_id', $data) ? '_ipn' . $data['ipn_id'] : '');

        if ($data['status'] >= 100) {
            if (!isset($data['address']) || !$data['address']) {
                Log::error('COINPAYMENTS FAILS 3.1', $data);
                return response('');
            }
            // $userAccount  - юзеровский кошелёк, получаем по адресу $data['address']
            $userAccount = $this->accountService->getAccountByAddress($data['address']);
            if (!$userAccount) {
                Log::error('COINPAYMENTS FAILS 3', $data);
                return response('');
            }
            if ($userAccount->currency->code !== $data['currency']) {
                Log::error('COINPAYMENTS FAILS 8', $data);
                return response('');
            }
            // Создаём трансфер - зачисляем юзеру монеты
            // $data['currency'] - валюта
            // $data['amount'] - количество монет
            try {
                $transfer = $this->transferService->makeDeposit($reference, $data['amount'], $userAccount);
            } catch (ClosedAccountException $e) {
                Log::error('COINPAYMENTS FAILS 4', $data);
                return response('');
            } catch (InsufficientFundsException $e) {
                Log::error('COINPAYMENTS FAILS 5', $data);
                return response('');
            } catch (InvalidAmountException $e) {
                Log::error('COINPAYMENTS FAILS 6', $data);
                return response('');
            } catch (AccountException $e) {
                Log::error('COINPAYMENTS FAILS 7', $data);
                return response('');
            }

            Log::info('COINPAYMENTS CONFIRMED DEPOSIT', $data);

            // Подтверждаем трансфер - создание транзакций
            $this->transferService->confirmTransfer($transfer);

            // Покупаем токены
            $this->transferService->buyTokens($userAccount->user);
        } else {
            Log::info('COINPAYMENTS UNCONFIRMED DEPOSIT', $data);
        }
        return response('');
    }
}
