<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Volume;
use Illuminate\Http\Request;

class VolumeController extends Controller
{
    public function index()
    {
        $volumes = Volume::all();
        return view('admin.volume', compact('volumes'));
    }

    public function store(Request $request)
    {
        $data = $request->all()['data'];
        $volumeIds = Volume::all()->pluck('id')->toArray();
        $gotIds = $data['ids'];

        $this->deleteVolumes($gotIds, $volumeIds);

        foreach ($data['volumes'] as $volume) {

            $foundVolume = Volume::find($volume['id']);

            if (!is_null($foundVolume)) {
                $foundVolume->update([
                    'eth' => $volume['eth'],
                    'discount' => $volume['discount']
                ]);

            } else {
                Volume::create([
                    'id' => $volume['id'],
                    'eth' => $volume['eth'],
                    'discount' => $volume['discount']
                ]);

            }
        }

    }

    public function deleteVolumes($gotIds, $issetIds)
    {
        $diff = array_diff($issetIds, $gotIds);

        foreach ($diff as $volumeId) {
            $volume = Volume::find($volumeId);

            if (!is_null($volume)) {
                $volume->delete();
            }
        }
    }
}
