<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Stage;
use App\Models\Accounts\Transfer;
use App\Models\Auth\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    public function show($id)
    {
        $user = User::with(['referrals', 'referrer'])->find($id);
        $transfers = $user->transfers;
        $accounts = $user->accounts;
        return view('admin.users.show', compact('transfers', 'user', 'accounts'));
    }
}
