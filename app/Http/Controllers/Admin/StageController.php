<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Stage;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StageController extends Controller
{

    public function index()
    {
        $stages = Stage::all();
        return view('admin.stage.index', compact('stages'));
    }

    public function edit($id)
    {
        $stage = Stage::find($id);
        return view('admin.stage.edit', compact('stage'));
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $stage = Stage::find($data['stage_id']);
        $data['begin_at'] = isset($data['begin_at']) && $data['begin_at'] ? Carbon::createFromFormat('Y-m-d\TH:i', $data['begin_at']) : null;
        $data['expire_at'] = isset($data['expire_at']) && $data['expire_at'] ? Carbon::createFromFormat('Y-m-d\TH:i', $data['expire_at']) : null;
        $stage->update($data);

        return redirect(route('stages'));
    }

    public function create(Request $request)
    {
        $stage = new Stage();
        return view('admin.stage.edit', compact('stage'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['begin_at'] = isset($data['begin_at']) && $data['begin_at'] ? Carbon::createFromFormat('Y-m-d\TH:i', $data['begin_at']) : null;
        $data['expire_at'] = isset($data['expire_at']) && $data['expire_at'] ? Carbon::createFromFormat('Y-m-d\TH:i', $data['expire_at']) : null;
        $stage = new Stage($data);
        $stage->save();

        return redirect(route('stage-edit', ['id' => $stage->id]));
    }

    public function delete(Stage $stage)
    {
        $stage->delete();
        return redirect(route('stages'));
    }
}
