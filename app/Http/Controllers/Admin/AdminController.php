<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Currency;
use App\Models\Accounts\Stage;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $stages = Stage::all();
        return view('admin.index', compact('stages'));
    }

    public function currencies()
    {
        $currencies = Currency::all();

        return view('admin.currencies', compact('currencies'));
    }

    public function acceptCurrency(Request $request)
    {
        $accepted = $request->has('checked') ? $request->get('checked') : null;
        $id = $request->has('id') ? $request->get('id') : null;

        if (!is_null($accepted) && !is_null($id)) {
            $currency = Currency::find($id);
            $currency->is_accepted = $accepted;
            $currency->save();

            return response()->json(['msg' => 'ok'], 200);
        } else {
            return response()->json(['msg' => 'error'], 422);

        }
    }


    public function activeStage(Request $request)
    {
        $active = $request->has('checked') ? $request->get('checked') : null;
        $id = $request->has('id') ? $request->get('id') : null;

        if (!is_null($active) && !is_null($id)) {

            $currentActiveStage = Stage::where('is_active', true)->first();

            if (!empty($currentActiveStage)) {
                $currentActiveStage->update(['is_active' => false]);
            }

            $stage = Stage::find($id);
            $stage->is_active = $active;
            $stage->save();

            return response()->json(['msg' => 'ok'], 200);
        } else {
            return response()->json(['msg' => 'error'], 422);

        }
    }
}
