<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Accounts\Transfer;

class TransferController extends Controller
{
    public function index()
    {
        $transfers = Transfer::all();
        return view('admin.transfers', compact('transfers'));
    }
}
