<?php


function get_real_ip() {
    return isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : \Illuminate\Support\Facades\Request::ip();
}
