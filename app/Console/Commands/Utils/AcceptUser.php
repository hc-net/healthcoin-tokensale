<?php
/**
 * Created by IntelliJ IDEA.
 * User: Exsmund
 * Date: 25.07.2018
 * Time: 1:18
 */

namespace App\Console\Commands\Utils;


use App\Models\Auth\User;
use Illuminate\Support\Facades\Validator;

trait AcceptUser
{

    protected $rules = [
        'email' => 'required|email',
    ];

    private function getUser()
    {
        $email = $this->argument('user');
        /** @var User $user */
        if (is_numeric($email)) {
            $user = User::find($email);
        } else {
            if (!$email) {
                $this->error('User email or ID is expected');
                return null;
            }

            $validator = Validator::make(['email' => $email], $this->rules);
            if ($validator->fails()) {
                $this->error("'$email' is wrong email");
                return null;
            }
            $user = User::where('email', $email)->first();
        }

        if (!$user) {
            $fieldName = (is_numeric($email) ? 'ID ' : 'email ') . $email;
            $this->error("User with $fieldName was not found");
            return null;
        }
        return $user;
    }
}
