<?php

namespace App\Console\Commands;

use App\Models\Accounts\Currency;
use App\Models\Accounts\Price;
use App\Services\CoinPaymentsService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SyncExchangeRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinpayments:syncexchangerates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import coins exchange rates from Coinpayments API';

    protected $paymentsService;

    /**
     * Create a new command instance.
     *
     * @param CoinPaymentsService $paymentsService
     */
    public function __construct(CoinPaymentsService $paymentsService)
    {
        parent::__construct();
        $this->paymentsService = $paymentsService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rates = $this->paymentsService->getExchangeRates();
        $btcId = Currency::where('code', 'BTC')->value('id');
        foreach ($rates as $code => $rate) {
            $currency = Currency::firstOrCreate(['code' => $code], [
                'name' => $rate['name'],
                'code' => $code,
                'type' => $rate['is_fiat'] ? Currency::TYPE_FIAT : Currency::TYPE_COIN,
            ]);
            $attributes = [
                'commodity_id' => $currency->id,
                'currency_id' => $btcId
            ];
            $price = Price::firstOrCreate($attributes, array_merge($attributes, [
                'date' => Carbon::createFromTimestampUTC($rate['last_update']),
                'value' => $rate['rate_btc'],
            ]));
            $price->date = Carbon::createFromTimestampUTC($rate['last_update']);
            $price->value = $rate['rate_btc'];
            $price->save();
        }
    }
}
