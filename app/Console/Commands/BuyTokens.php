<?php

namespace App\Console\Commands;

use App\Console\Commands\Utils\AcceptUser;
use App\Services\TransferService;
use Illuminate\Console\Command;

class BuyTokens extends Command
{

    use AcceptUser;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokens:buy {user : Email or ID of user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $transferService;

    /**
     * Create a new command instance.
     *
     * @param TransferService $transferService
     */
    public function __construct(TransferService $transferService)
    {
        parent::__construct();
        $this->transferService = $transferService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $user = $this->getUser();
        if (!$user) {
            return;
        }

        $this->transferService->buyTokens($user);
    }
}
