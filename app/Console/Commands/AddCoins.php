<?php

namespace App\Console\Commands;

use App\Console\Commands\Utils\AcceptUser;
use App\Exceptions\Accounts\AccountException;
use App\Exceptions\Accounts\ClosedAccountException;
use App\Exceptions\Accounts\InsufficientFundsException;
use App\Exceptions\Accounts\InvalidAmountException;
use App\Models\Accounts\Currency;
use App\Models\Auth\User;
use App\Services\AccountService;
use App\Services\PriceService;
use App\Services\TransferService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AddCoins extends Command
{

    use AcceptUser;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coins:add {user : Email or ID of user} {amount} {currency}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add coins to user';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $stageService;
    protected $transferService;
    protected $accountService;
    protected $priceService;

    public function __construct(
        TransferService $transferService,
        PriceService $priceService,
        AccountService $accountService
    ) {
        parent::__construct();
        $this->transferService = $transferService;
        $this->accountService = $accountService;
        $this->priceService = $priceService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $user = $this->getUser();
        if (!$user) {
            return;
        }

        $amount = $this->argument('amount');
        if (!is_numeric($amount)) {
            $this->error('Wrong amount');
            return;
        }
        $currencyCode = $this->argument('currency');
        if (!$currencyCode) {
            $this->error('Currency code is expecting');
            return;
        }
        $currency = Currency::where('code', $currencyCode)->first();
        if (!$currency) {
            $this->error("Currency with code $currencyCode is not found");
            return;
        }
        $account = $user->getBankAccount($currency);
        if (!$account) {
            $this->error("User account is not found");
            return;
        }
        $reference = 'test_deposit_' . Hash::make(time());
        try {
            $transfer = $this->transferService->makeDeposit($reference, $amount, $account);
        } catch (ClosedAccountException $e) {
            $this->error('Close account');
            return;
        } catch (InsufficientFundsException $e) {
            $this->error('Insufficient funds');
            return;
        } catch (InvalidAmountException $e) {
            $this->error('Invalid amount');
            return;
        } catch (AccountException $e) {
            $this->error('Account exception');
            return;
        }
        $this->info('Deposit - ok');
        $this->transferService->confirmTransfer($transfer);
        $this->info('Confirm - ok');
        $this->transferService->buyTokens($user);
        $this->info('Tokens - ok');
        $this->info('Done');
    }
}
