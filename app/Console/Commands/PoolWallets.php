<?php

namespace App\Console\Commands;

use App\Exceptions\Accounts\CurrencyNotExists;
use App\Exceptions\Accounts\UndefinedTypeAccountException;
use App\Models\Accounts\Account;
use App\Models\Accounts\Currency;
use App\Services\AccountService;
use App\Services\CoinPaymentsService;
use App\Services\CurrencyService;
use Illuminate\Console\Command;

class PoolWallets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wallets:pool';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill wallets pool';


    private $accountService;
    private $currencyService;
    private $coinPaymentsPaymentsService;

    /**
     * Create a new command instance.
     *
     * @param AccountService $accountService
     * @param CurrencyService $currencyService
     * @param CoinPaymentsService $coinPaymentsPaymentsService
     */
    public function __construct(
        AccountService $accountService,
        CurrencyService $currencyService,
        CoinPaymentsService $coinPaymentsPaymentsService
    )
    {
        parent::__construct();
        $this->accountService = $accountService;
        $this->currencyService = $currencyService;
        $this->coinPaymentsPaymentsService = $coinPaymentsPaymentsService;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $acceptedCoinCurrencies = $this->currencyService->getAcceptedCoinCurrencies();
        $currenciesIds = $acceptedCoinCurrencies->pluck('id');
        $existsAccountsCount = Account::whereNull('user_id')
            ->whereNotNull('wallet')
            ->where('type', Account::TYPE_BANK_PAYMENT)
            ->whereIn('currency_id', $currenciesIds)->count();
        $poolSize = config('app.wallet_pool_size');
        $totalPoolSize = $poolSize * $acceptedCoinCurrencies->count();
        $totalCount = $totalPoolSize - $existsAccountsCount;

        $bar = $this->output->createProgressBar($totalCount);
        foreach ($acceptedCoinCurrencies as $currency) {
            $count = $poolSize - Account::whereNull('user_id')
                ->whereNotNull('wallet')
                ->where('currency_id', $currency->id)->count();
            for ($i = 0; $i < $count; $i++) {
                $wallet = $this->coinPaymentsPaymentsService->createWallet($currency);
                if (is_string($wallet)) {
                    $this->error("$currency: '$wallet'");
                } else {
                    try {
                        $this->accountService->create([
                            'wallet' => $wallet,
                            'currency' => $currency,
                            'type' => Account::TYPE_BANK_PAYMENT
                        ]);
                    } catch (CurrencyNotExists $e) {
                        $this->error("Currency $currency not exists");
                    } catch (UndefinedTypeAccountException $e) {
                        $this->error("Wrong account type");
                    }
                    $bar->advance();
                }
            }
        }

        $bar->finish();

        $this->info('');
        $this->info('Done');
    }
}
