<?php

namespace App\Jobs;

use App\Exceptions\Crowdsale\NoBlockHashException;
use App\Models\Accounts\Transfer;
use App\Services\EthereumService;
use App\Services\TransferService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SendTokens implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $transfer;


    /**
     * Create a new job instance.
     * @param Transfer $transfer
     */
    public function __construct(Transfer $transfer)
    {
        $this->transfer = $transfer;
    }

    /**
     * Execute the job.
     *
     * @param EthereumService $ethereumService
     * @param TransferService $transferService
     * @return void
     * @throws \App\Exceptions\EthereumServiceException
     */
    public function handle(EthereumService $ethereumService, TransferService $transferService)
    {
        if ($this->transfer->reference) {
            if (config('app.debug')) {
                Log::info("SendTokens: transfer already has reference", [$this->transfer->id]);
            }
            return;
        }

        $user = $this->transfer->destination->user;
        if (!$user->ether_account) {
            if (config('app.debug')) {
                Log::info("SendTokens: user does not have a ERC-20 wallet", [$this->transfer->id]);
            }
            return;
        }
        if (!$user->ether_account->wallet) {
            if (config('app.debug')) {
                Log::info("SendTokens: user has empty ERC-20 wallet", [$this->transfer->id]);
            }
            return;
        }

        $address = $user->ether_account->wallet;
        $amount = $this->transfer->amount;
        if ($amount <= 0) {
            if (config('app.debug')) {
                Log::info("SendTokens: the amount is zero", [$this->transfer->id]);
            }
            return;
        }

        $result = $ethereumService->sendTokens($address, $amount);

        if (!isset($result['transactionHash'])) {
            Log::error("SendTokens: result does not have transactionHash", [$this->transfer->id]);
            $this->fail(new NoBlockHashException(''));
        }
        $this->transfer->reference = $result['transactionHash'];
        $this->transfer->save();

        $transferService->confirmTransfer($this->transfer);
    }
}
