<?php

namespace App\Mail;

use App\Models\Accounts\WalletConfirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmWallet extends Mailable
{
    use Queueable, SerializesModels;
    use MailTools;
    public $confirmation;

    /**
     * Create a new message instance.
     *
     * @param WalletConfirmation $confirmation
     */
    public function __construct(WalletConfirmation $confirmation)
    {
        $this->confirmation = $confirmation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = [
            'confirmation' => $this->confirmation,
            'noClickableUrl' => $this->noClickable($this->confirmation->confirmUrl),
        ];
        return $this
            ->view('email.confirm_wallet')
            ->with($params)
            ->to($this->confirmation->user->email)
            ->subject('Confirm your wallet');
    }
}
