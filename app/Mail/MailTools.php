<?php

namespace App\Mail;

trait MailTools {

    /**
     * Сделать ссылку не кликабельной в письмах
     * @param string $string
     * @return string
     */
    public function noClickable($string)
    {
        $url = implode('.', array_map(
            function ($s) {
                return "<span>$s</span>";
            },
            preg_split("/\./", $string, -1, PREG_SPLIT_DELIM_CAPTURE)
        ));
        $url = implode(':', array_map(
            function ($s) {
                return "<span>$s</span>";
            },
            preg_split("/\:/", $url, -1, PREG_SPLIT_DELIM_CAPTURE)
        ));
        return $url;
    }
}