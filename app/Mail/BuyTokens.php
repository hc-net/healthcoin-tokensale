<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BuyTokens extends Mailable
{
    use Queueable, SerializesModels;

    protected $tokensCount;
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $tokensCount)
    {
        $this->tokensCount = $tokensCount;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->user->email)
            ->from('no-reply@healthcoin-tokensale.io','Healthcoin')
            ->view('email.buy_tokens')->with(['tokens_count' => $this->tokensCount, 'user' => $this->user])
            ->subject('You bought tokens');
    }
}
