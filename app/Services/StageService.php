<?php

namespace App\Services;


use App\Models\Accounts\Stage;
use Carbon\Carbon;

class StageService {

    public function __construct(
    ) {
    }


    /**
     * @return Stage
     */
    public function getCurrentStage() {
        $now = Carbon::now();
        return Stage::where('expire_at', '>=', $now)->orderBy('expire_at')->first();
    }
}
