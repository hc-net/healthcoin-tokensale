<?php

namespace App\Services;


use App\Models\Accounts\Currency;
use App\Services\API\CoinPaymentsAPI;
use Illuminate\Support\Facades\Log;

class CoinPaymentsService {

    protected $apiClient;

    public function __construct() {
        $this->apiClient = new CoinPaymentsAPI();
    }

    public function createTransaction($amount, $currency, $address='', $buyerEmail='') {
        $ipnUrl = config('services.coinpayments')['ipn_host'] . route('coinpayments.transactionIpnUrl', [], false);
        $response = $this->apiClient->createTransactionSimple(
            $amount,
            $currency,
            $currency,
            $address,
            $ipnUrl,
            $buyerEmail
        );
        Log::info(array($amount, $address, $currency, $response));
        $result = ['error' => '', 'redirect' => '', 'reference' => ''];
        if (array_has($response, 'error') && $response['error'] !== 'ok') {
            $result['error'] = $response['error'];
        }
        if (array_has($response, 'result.status_url') && $response['result']['status_url']) {
            $result['redirect'] = $response['result']['status_url'];
        }
        if (array_has($response, 'result.txn_id') && $response['result']['txn_id']) {
            $result['reference'] = $response['result']['txn_id'];
        }
        return $result;
    }

    function createWallet(Currency $currency) {
        $ipnUrl = config('services.coinpayments')['ipn_host'] . route('coinpayments.depositIpnUrl', [],false);
        $response = $this->apiClient->getCallbackAddress($currency->code, $ipnUrl);
        try {
            if (array_has($response, ['error']) && $response['error'] === 'ok') {
                $result = $response['result'];
            } else {
                $result = $response['error'];
            }
        } catch (\Exception $e) {
            dd($response);
        }
        return $result;
    }

    function getExchangeRates() {
        $rates = $this->apiClient->getRates(false);
        return $rates['result'];
    }

    function getTransaction($currency, $txid)
    {
        $response = $this->apiClient->getTransaction($txid);
        Log::info($response);
        return $response['result'];
    }
}
