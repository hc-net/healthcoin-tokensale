<?php


namespace App\Services;

use App\Exceptions\EthereumServiceException;
use GuzzleHttp\Client as GuzzleHttp;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class EthereumService
{
    /**
     * Http Client.
     *
     * @var \GuzzleHttp\Client
     */
    protected $client = null;
    protected $cache;

    public function __construct()
    {
        $config = config('services.ethereum');
        // construct client
        $this->client = new GuzzleHttp([
            'base_uri' => "${config['scheme']}://${config['host']}:${config['port']}",
        ]);
        $this->cache = Cache::store();
    }

    /**
     * @param $address
     * @param $value
     * @return mixed
     * @throws EthereumServiceException
     */
    public function sendTokens($address, $value)
    {
        // Convert to Wei
        $value = bcmul($value, pow(10, 18), 0);
        $data = [
            'address' => $address,
            'value' => $value,
        ];
        if (config('app.debug')) {
            Log::info("contract sendTokens", $data);
        }
        $response = $this->client->get('/sendtokens', ['query' => $data]);
        $result = json_decode($response->getBody(), true);
        if (array_key_exists('error', $result)) {
            Log::error("contract sendTokens, error", $result);
            throw new EthereumServiceException('Error accessing geth client proxy: ' . json_encode($result));
        }
        if (config('app.debug')) {
            Log::info("contract sendTokens, result", $result);
        }
        return $result;
    }


}
