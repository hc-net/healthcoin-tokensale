<?php

namespace App\Services;


use App\Models\Accounts\Currency;
use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class CurrencyService {

    protected $tokenCode;
    protected $tokenName;
    protected $cm2TokenCode;
    protected $cm2TokenName;

    public function __construct(
    ) {
        $this->tokenCode = config('app.current_token');
        $this->tokenName = config('app.current_token_name');
    }

    /**
     * @return Currency
     */
    public function getTokenCurrency()
    {
        return Currency::firstOrCreate(
            ['code' => $this->tokenCode],
            ['name' => $this->tokenName]
        );
    }

    /**
     * @return Currency[]
     */
    public function getAcceptedCoinCurrencies() {
        return Currency::where('is_accepted', true)->where('type', Currency::TYPE_COIN)->get();
    }

}
