<?php

namespace App\Services;


use App\Exceptions\Accounts\CurrencyNotExists;
use App\Exceptions\Accounts\UndefinedTypeAccountException;
use App\Models\Accounts\Account;
use App\Models\Accounts\AccountType;
use App\Models\Accounts\Currency;
use App\Models\Accounts\Transfer;
use App\Models\Auth\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AccountService {

    public function __construct(
    ) {
    }

    /**
     * @param $address
     * @return Account|null
     */
    public function getAccountByAddress($address)
    {
        return Account::where('wallet', $address)->first();
    }

    /**
     * @param array $attributes
     * @return Account
     * @throws UndefinedTypeAccountException
     * @throws CurrencyNotExists
     */
    public function create(array $attributes) {
        $user = array_has($attributes, 'user') ? $attributes['user'] : null;
        $currency = $attributes['currency'];
        if (is_string($currency)) {
            $currency = Currency::where(['code' => $currency])->first();
        }
        if (!$currency) {
            throw new CurrencyNotExists('Account type is expected');
        }
        $wallet = array_has($attributes, 'wallet') ? $attributes['wallet'] : null;
        $status = array_has($attributes, 'status') ? $attributes['status'] : Account::STATUS_OPEN;
        if (!array_has($attributes, 'type')) {
            throw new UndefinedTypeAccountException('Account type is expected');
        }
        $type = $attributes['type'];
        $account = new Account([
            'name' => $user ? "$currency->code $type [$user->email]" : "$currency->code wallet",
            'status' => $status,
            'type' => $type,
        ]);
        if ($user) {
            $account->user()->associate($user);
        }
        $account->currency()->associate($currency);
        $account->save();

        if ($wallet) {
            if (array_has($wallet, 'address')) {
                $account->wallet = $wallet['address'];
            }
            if (array_has($wallet, 'pubkey')) {
                $account->pubkey = $wallet['pubkey'];
            }
            if (array_has($wallet, 'dest_tag')) {
                $account->dest_tag = $wallet['dest_tag'];
            }
            $account->save();
        }
        return $account;
    }

    public function getSourceCoinAccount(Currency $currency) {
        $account = Account::where([
            'type' => Account::TYPE_COIN_SOURCE,
            'currency_id' => $currency->id,
        ])->first();

        if ($account) {
            return $account;
        }

        $account = new Account([
            'name' => "$currency->code coin source",
            'type' => Account::TYPE_COIN_SOURCE,
            'status' => Account::STATUS_OPEN,
            'credit_limit' => null,
        ]);
        $account->currency()->associate($currency);
        $account->save();

        return $account;
    }

    public function getSellerCoinAccount(Currency $currency)
    {
        $account = Account::where([
            'type' => Account::TYPE_SELLER_COIN,
            'currency_id' => $currency->id,
        ])->first();

        if ($account) {
            return $account;
        }

        $account = new Account([
            'name' => "$currency->code seller coin",
            'type' => Account::TYPE_SELLER_COIN,
            'status' => Account::STATUS_OPEN,
        ]);
        $account->currency()->associate($currency);
        $account->save();

        return $account;
    }

    public function getSourceTokenAccount(Currency $currency) {
        $account = Account::where([
            'type' => Account::TYPE_TOKEN_SOURCE,
            'currency_id' => $currency->id,
        ])->first();

        if ($account) {
            return $account;
        }

        $account = new Account([
            'name' => "$currency->code token source",
            'type' => Account::TYPE_TOKEN_SOURCE,
            'status' => Account::STATUS_OPEN,
            'credit_limit' => null,
        ]);
        $account->currency()->associate($currency);
        $account->save();

        return $account;
    }

    public function getUserTokenAccount(User $user, Currency $currency)
    {
        $account = $user->accounts()
            ->where([
                'type' => Account::TYPE_USER_TOKEN,
                'currency_id' => $currency->id,
            ])
            ->first();

        if ($account) {
            return $account;
        }

        $account = new Account([
            'name' => "$currency->code user token [$user->email]",
            'type' => Account::TYPE_USER_TOKEN,
            'status' => Account::STATUS_OPEN,
        ]);
        $account->user()->associate($user);
        $account->currency()->associate($currency);
        $account->save();

        return $account;
    }

    public function isValidETHAddress($address) {
        $result = null;
        $cmd = 'python -c "from eth_utils.address import is_address; print is_address(\"'. $address . '\")"';
        exec($cmd, $result);
        return count($result) > 0 && $result[0] === 'True';
    }

    /**
     * @param User $user
     * @return int|string
     * Считает коиновый баланс юзера в эфировом эквиваленте
     */
    public function getUserEthBalance(User $user)
    {
        $accounts = $user->paymentsAccounts;
        $this->updateBalance($accounts);
        $totalEth = 0;
        /** @var PriceService $priceService */
        $priceService = app(PriceService::class);
        foreach ($accounts as $account) {
            $balance = $account->actual_balance;
            if (bccomp($balance, 0, 20) > 0) {
                $price = $priceService->getPrice($account->currency->code, 'ETH');
                $totalEth = bcadd($totalEth, bcmul($balance, $price, 20), 20);
            }
        }
        return $totalEth;
    }

    /**
     * @param Account[] $accounts
     */
    public function updateBalance($accounts)
    {
        $ids = [];
        foreach ($accounts as $account) {
            $ids[] = $account->id;
        }
        DB::table('accounts')
            ->leftJoin(
                DB::raw('(SELECT account_id, sum(amount) total_amount FROM transactions GROUP BY account_id) transactions_amount'),
                DB::raw('transactions_amount.account_id'), '=', 'accounts.id'
            )
            ->whereIn('accounts.id', $ids)
            ->update([
                'balance' => DB::raw('IF(transactions_amount.total_amount IS NULL, 0, transactions_amount.total_amount)'),
                'updated_at' => Carbon::now(),
            ]);
        foreach ($accounts as $account) {
            $account->refresh();
        }
    }

    public function getCrowdsaleAddress()
    {
        $account = new Account();
        $account->wallet = env('CROWDSALE_ADDRESS');
        return $account;
    }

}
