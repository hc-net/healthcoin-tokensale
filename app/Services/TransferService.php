<?php

namespace App\Services;


use App\Exceptions\Accounts\AccountException;
use App\Exceptions\Accounts\ClosedAccountException;
use App\Exceptions\Accounts\InsufficientFundsException;
use App\Exceptions\Accounts\InvalidAmountException;
use App\Exceptions\Accounts\UserNotFoundException;
use App\Jobs\SendTokens;
use App\Mail\BuyTokens;
use App\Models\Accounts\Account;
use App\Models\Accounts\Stage;
use App\Models\Accounts\Transaction;
use App\Models\Accounts\Transfer;
use App\Models\Auth\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class TransferService {
    protected $accountService;
    protected $currencyService;
    protected $priceService;
    protected $promocodeService;
    protected $referralService;
    protected $stageService;

    protected $scale = 20;
    protected $googleAnalyticsService;

    public function __construct(
        AccountService $accountService,
        CurrencyService $currencyService,
        PriceService $priceService,
        StageService $stageService
    ) {
        $this->accountService = $accountService;
        $this->currencyService = $currencyService;
        $this->priceService = $priceService;
        $this->stageService = $stageService;
    }

    /**
     * @param Account $source
     * @param Account $destination
     * @param $amount
     * @param User|null $user
     * @throws AccountException
     * @throws ClosedAccountException
     * @throws InsufficientFundsException
     * @throws InvalidAmountException
     */
    private function verifyTransfer(Account $source, Account $destination, $amount, User $user=null) {
        if ($amount <= 0) {
            throw new InvalidAmountException("Invalid amount: ${amount}");
        }
        if (!$source->isOpen()) {
            throw new ClosedAccountException("Source account " . $source->id . " is closed.");
        }
        if (!$source->canBeAuthorisedBy($user)) {
            throw new AccountException("This user is not authorised to make transfers from this account.");
        }
        if (!$destination->isOpen()) {
            throw new ClosedAccountException("Destination account " . $destination->id . " is closed.");
        }
        if (!$source->isDebitPermitted($amount)) {
            throw new InsufficientFundsException("Unable to debit ${amount} from account #" . $source->id);
        }
    }

    /**
     * Создаёт трансфер для указанного количества токенов.
     *
     * @param string $reference
     * @param $amount
     * @param Account $userAccount
     * @return Transfer
     * @throws AccountException
     * @throws ClosedAccountException
     * @throws InsufficientFundsException
     * @throws InvalidAmountException
     */
    public function makeDeposit($reference, $amount, Account $userAccount)
    {
        // Ежели указан айди транзакции, то дубликат не создаём
        if ($reference && Transfer::where('reference', $reference)->exists()) {
            return Transfer::where('reference', $reference)->first();
        }
        // $source - это неведомый источник монет
        $source = $this->accountService->getSourceCoinAccount($userAccount->currency);
        $user = $userAccount->user;

        $this->verifyTransfer($source, $userAccount, $amount, $user);
        // создаём трансфер без транзакций
        $currencyCode = $userAccount->currency->code;
        $transfer = $this->makeTransfer(
            Transfer::TYPE_COIN_BALANCE,
            $amount,
            $source,
            $userAccount,
            "Deposit $currencyCode [$user->email]",
            $user
        );
        $transfer->reference = $reference;
        $transfer->save();
        return $transfer;
    }

    public function makeTransfer($type, $amount, $source, $destination, $description, User $user = null) {
        $transfer = new Transfer([
            'type' => $type,
            'ratio' => 1,
            'amount' => $amount,
            'description' => $description,
        ]);
        if ($source) {
            $transfer->source()->associate($source);
        }
        if ($destination) {
            $transfer->destination()->associate($destination);
        }
        if ($user) {
            $transfer->user()->associate($user);
        }
        return $transfer;
    }

    /**
     * Подтверждение трансфера, ака создание транзакций для оного
     *
     * @param Transfer|string $transfer
     * @return Transfer
     */
    public function confirmTransfer($transfer)
    {
        if (is_string($transfer)) {
            $transfer = Transfer::where('reference', $transfer)->first();
        }

        $this->makeTransactionsForTransfer($transfer);
        $this->accountService->updateBalance([$transfer->source, $transfer->destination]);

        return $transfer;
    }

    protected function makeTransactionsForTransfer(Transfer $transfer)
    {
        if ($transfer->transactions()->count() === 2) {
            return;
        }
        DB::transaction(function() use ($transfer) {
            $this->makeTransaction(bcmul(-1, $transfer->amount, 20), $transfer, $transfer->source)->save();
            $this->makeTransaction($transfer->amount, $transfer, $transfer->destination)->save();
        }, 10);
    }

    protected function makeTransaction($amount, $transfer, $account)
    {
        $transaction = new Transaction([
            'amount' => $amount
        ]);
        if ($transfer) {
            $transaction->transfer()->associate($transfer);
        }
        if ($account) {
            $transaction->account()->associate($account);
        }
        return $transaction;
    }

    /**
     * Купить токену пользователю на весь его баланс
     * @param User $user
     */
    public function buyTokens(User $user)
    {
        $totalEth = $this->accountService->getUserEthBalance($user);

        if (bccomp($totalEth, 0, 20) <= 0) {
            return;
        }

        $stage = $this->stageService->getCurrentStage();
        if (bccomp($totalEth, $stage->min, 20) < 0) {
            return;
        }
        try {
            DB::beginTransaction();

            $coinTransfers = [];
            $totalEth = 0;
            foreach ($user->paymentsAccounts as $account) {
                $balance = $account->actual_balance;
                if (bccomp($balance, 0, $this->scale) <= 0) {
                    continue;
                }

                $price = $this->priceService->getPrice($account->currency->code, 'ETH');
                $totalEth = bcadd($totalEth, bcmul($balance, $price, 20), 20);

                $sellerCoinAccount = $this->accountService->getSellerCoinAccount($account->currency);
                $coinTransfer = $this->createSpendingCoinsTransfer(
                    $account,
                    $sellerCoinAccount,
                    $balance,
                    $stage
                );
                $coinTransfers[] = $coinTransfer;
                $this->confirmTransfer($coinTransfer);
            }
            $tokensTransfer = $this->createTokenPurchaseTransfer($coinTransfers);

            SendTokens::dispatch($tokensTransfer);

            Mail::queue(new BuyTokens($user, $tokensTransfer->amount));

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('buyTokens: ' . $e->getMessage());
        }
    }

    /**
     * @param Account $source
     * @param Account $destination
     * @param $amount
     * @param Stage $stage
     * @return Transfer
     * @throws AccountException
     * @throws ClosedAccountException
     * @throws InsufficientFundsException
     * @throws InvalidAmountException
     */
    protected function createSpendingCoinsTransfer(Account $source, Account $destination, $amount, Stage $stage) {
        $user = $source->user;
        $currency = $source->currency;
        $this->verifyTransfer($source, $destination, $amount, $user);
        $price = $this->priceService->getPrice('ETH', $source->currency->code);
        $bonusRatio = bcdiv(100 + $stage->bonus, 100, $this->scale);

        $transfer = $this->makeTransfer(
            Transfer::TYPE_COIN_SPENDING,
            $amount,
            $source,
            $destination,
            "Spending $currency->code [$user->email]",
            $user
        );
        $transfer->ratio = $bonusRatio;
        $transfer->price = $price;
        $transfer->stage()->associate($stage);
        $transfer->save();
        return $transfer;
    }


    /**
     * Принимает трансферы списания коинов, создаёт один трансфер начисления токенов
     * @param Transfer[] $transfers
     * @return Transfer|null
     * @throws UserNotFoundException
     */
    protected function createTokenPurchaseTransfer($transfers)
    {
        /** @var User $user */
        $user = null;
        $tokenCurrency = $this->currencyService->getTokenCurrency();
        $tokenSource = $this->accountService->getSourceTokenAccount($tokenCurrency);
        $ethAmount = 0;
        foreach ($transfers as $transfer) {
            $user = $transfer->user;
            $ethAmount = bcadd($ethAmount, bcdiv($transfer->amount, $transfer->price, $this->scale), $this->scale);
        }
        if (!$user) {
            Log::error('createTokenPurchaseTransfer FAILS');
            throw new UserNotFoundException('User is required');
        }
        $tokenDestination = $this->accountService->getUserTokenAccount($user, $tokenCurrency);
        $tokenPrice = $this->priceService->getTokenPriceInETH();
        $tokensAmountWoBonuses = bcdiv($ethAmount, $tokenPrice, $this->scale);
        $stage = $this->stageService->getCurrentStage();

        $bonuses = [
            Transfer::ETH_AMOUNT => $ethAmount,
            Transfer::BONUS_TYPE_STAGE => 0,
            Transfer::BONUS_TYPE_VOLUME => 0,
        ];
        $bonus = 0;
        if ($stage) {
            $bonuses[Transfer::BONUS_TYPE_STAGE] = $stage->bonus;
            $bonus += $stage->bonus;
        }

        $amount_bonuses_array = []; // TODO amount bonuses
        $amountBonus = 0;
        if (is_array($amount_bonuses_array)) {
            foreach ($amount_bonuses_array as $amount) {
                if ($amount[0] > $ethAmount) {
                    break;
                }
                $amountBonus = $amount[1];
            }
        }
        $bonus += $amountBonus;
        $bonuses[Transfer::BONUS_TYPE_VOLUME] = $amountBonus;
        $ratio = (100 + $bonus) / 100;

        $tokensAmountWithBonuses = bcmul($tokensAmountWoBonuses, $ratio, $this->scale);

        $tokensTransfer = $this->makeTransfer(
            Transfer::TYPE_TOKEN_PURCHASE,
            $tokensAmountWithBonuses,
            $tokenSource,
            $tokenDestination,
            "Tokens purchase [$user->email]",
            $user
        );
        $tokensTransfer->bonuses = $bonuses;
        $tokensTransfer->ratio = $ratio;
        $tokensTransfer->price = $tokenPrice;
        if ($stage) {
            $tokensTransfer->stage()->associate($stage);
        }
        $tokensTransfer->save();
        foreach ($transfers as $transfer) {
            $transfer->parent()->associate($tokensTransfer);
            $transfer->save();
        }
        return $tokensTransfer;
    }
}
