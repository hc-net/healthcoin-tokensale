<?php

namespace App\Services;



use App\Models\Accounts\Currency;
use App\Models\Accounts\Price;
use App\Models\Accounts\Stage;
use Carbon\Carbon;

class PriceService {

    public function __construct(
    ) {
    }

    /**
     * @param Stage $stage
     * @param $currency
     * @return float
     * @throws \Exception
     */
    public function getPriceForStage(Stage $stage, $currency) {
        if (!$stage->priceCurrency || !$stage->token_price) {
            throw new \Exception('Stage price_currency_id and price fields are empty!');
        }
        if ($stage->priceCurrency->code == $currency) {
            return $stage->token_price;
        }
        $mainCurrencyPriceInBTC = $this->getPriceInCurrency('BTC', $stage->priceCurrency->code);
        // Курс выбранной пользователем валюты в BTC
        $currentCurrencyPriceInBTC = $this->getPriceInCurrency('BTC', $currency);
        // Вычисляем цену токена в BTC
        $tokenPriceInBTC = bcmul($stage->token_price, $mainCurrencyPriceInBTC, 20);
        // Вовзращаем цену токена в выбранной пользователем валюте
        return bcdiv($tokenPriceInBTC, $currentCurrencyPriceInBTC, 20);
    }

    public function getTokenPriceInBTC(Stage $stage)
    {
        // Курс основной валюты в BTC
        $mainCurrencyPriceInBTC = $this->getPriceInCurrency('BTC', $stage->priceCurrency->code);
        // Вычисляем цену токена в BTC
        return bcmul($stage->token_price, $mainCurrencyPriceInBTC, 20);
    }

    public function getTokenPriceInETH()
    {
        return config('app.token_price_eth');
    }

    public function getPriceInCurrency($currency, $commodity) {
        return Price::where('date', '<', Carbon::now())
            ->whereHas('commodity', function ($q) use ($commodity) {
                $q->where('code', $commodity);
            })->whereHas('currency', function ($q) use ($currency) {
                $q->where('code', $currency);
            })->orderBy('date', 'desc')->value('value');
    }

    public function getPrice($commodity, $currency) {
        $tokenPriceInBTC = $this->getPriceInCurrency('BTC', $commodity);
        $coinPriceInBTC = $this->getPriceInCurrency('BTC', $currency);
        return bcdiv($tokenPriceInBTC, $coinPriceInBTC, 20);
    }

}
