<?php

namespace App\Services;
use App\Models\Accounts\Volume;

class VolumeService {

    public function __construct(
    ) {
    }


    /**
     * @return array
     */

    public static function getVolumeDiscount()
    {
        $discounts = [];

        $volumes = Volume::all();
        foreach ($volumes as $volume) {
            $discounts[] = [$volume->eth, $volume->discount];
        }

        return $discounts;

    }
}
