<?php

namespace App\Observers;

use App\Models\Auth\User;
use Illuminate\Support\Str;

class UserObserver
{

    public function created(User $user)
    {
        //
    }

    public function creating(User $user)
    {
        if (!$user->referral_code) {
            $user->referral_code = Str::random(50);
        }
    }
}
