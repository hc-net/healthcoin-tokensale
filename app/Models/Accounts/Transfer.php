<?php

namespace App\Models\Accounts;

use App\Models\Auth\User;
use App\Models\Accounts\Stage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property User user              юзер-автор транзакции
 * @property Account source         кошелёк-источник
 * @property Account destination    кошелёк-цель
 * @property float amount           пересылаемое количество
 * @property string reference       айди транзакции из системы платежей
 * @property float price            обменный курс
 * @property float ratio            бонусный коэффициент. tokens amount = ( coins amount / price ) * ratio
 * @property integer user_id
 * @property integer id
 * @property Carbon created_at
 * @property boolean is_confirmed
 * @property string type
 * @property Transaction[] transactions
 * @property Transfer[] children
 * @property Stage stage
 * @property string country
 * @property Transfer parent
 * @property string bonuses
 */
class Transfer extends Model
{
    const TYPE_COIN_BALANCE = 'coin_balance';
    const TYPE_COIN_SPENDING = 'coin_spending';
    const TYPE_TOKEN_PURCHASE = 'token_purchase';

    const TYPES = [
        self::TYPE_COIN_BALANCE,
        self::TYPE_COIN_SPENDING,
        self::TYPE_TOKEN_PURCHASE,
    ];

    const ETH_AMOUNT = 'ethAmount';

    const BONUS_TYPE_STAGE = 'stage';
    const BONUS_TYPE_PROMOCODE = 'promocode';
    const BONUS_TYPE_REFERRAL = 'referral';
    const BONUS_TYPE_VOLUME = 'volume';

    const BONUS_TYPES_VERBOSE = [
        self::BONUS_TYPE_STAGE => 'Stage bonus',
        self::BONUS_TYPE_PROMOCODE => 'Promocode bonus',
        self::BONUS_TYPE_REFERRAL => 'Referral bonus',
        self::BONUS_TYPE_VOLUME => 'Volume bonus',
    ];

    protected $fillable = [
        'amount',
        'reference',
        'description',
        'price',
        'parent_id',
        'ratio',
        'type',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function source() {
        return $this->belongsTo(Account::class, 'source_id');
    }

    public function destination() {
        return $this->belongsTo(Account::class, 'destination_id');
    }

    public function stage() {
        return $this->belongsTo(Stage::class);
    }

    public function transactions() {
        return $this->hasMany(Transaction::class);
    }

    public function parent() {
        return $this->belongsTo(Transfer::class);
    }

    public function children()
    {
        return $this->hasMany(Transfer::class, 'parent_id');
    }

    public function getIsConfirmedAttribute()
    {
        return $this->transactions()->exists();
    }

    public function getBonusesAttribute()
    {
        return json_decode($this->attributes['bonuses'], true);
    }

    public function setBonusesAttribute($bonuses)
    {
        $this->attributes['bonuses'] = is_string($bonuses) ?$bonuses : json_encode($bonuses);
    }
}
