<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Volume extends Model
{
    protected $fillable = [
        'eth', 'discount'
    ];
}
