<?php

namespace App\Models\Accounts;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Stage
 *
 * @property int id
 * @property string name
 * @property int number
 * @property float bonus
 * @property float min
 * @property float max
 * @property Carbon begin_at
 * @property Carbon expire_at
 */
class Stage extends Model
{
    protected $fillable = [
        'name',
        'bonus',
        'min',
        'max',
        'begin_at',
        'expire_at',
        'is_active',
        'month',
    ];

    protected $dates = [
        'begin_at',
        'expire_at',
    ];
}
