<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * @property integer id
 * @property string code
 * @property mixed name
 */
class Currency extends Model
{

    const TYPE_COIN = 'coin';
    const TYPE_FIAT = 'fiat';
    const TYPE_TOKEN = 'token';

    const TYPES = [
        self::TYPE_COIN,
        self::TYPE_FIAT,
        self::TYPE_TOKEN,
    ];

    protected $table = 'currencies';

    protected $fillable = [
        'name',
        'code',
        'type',
        'is_accepted'
    ];

    public function __toString()
    {
        return $this->code;
    }

}
