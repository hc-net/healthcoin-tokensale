<?php

namespace App\Models\Accounts;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use Stevebauman\Location\Position;
use Stevebauman\Location\Facades\Location;

class WalletConfirmation extends Model
{
    protected $fillable = ['token', 'address', 'user_id', 'change_ip', 'confirm_ip'];
    protected $dates = ['created_at', 'confirmed_at'];

    public $timestamps = false;

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function getConfirmUrlAttribute() {
        return route('confirm_wallet', [$this->token], true);
    }

    public function getLocationAttribute() {
        /** @var Position $location_data */
        $location_data = Location::get($this->change_ip);
        $country = $location_data->countryName ? $location_data->countryName : $location_data->countryCode;
        $location = "Country: $country";
        if ($location_data->cityName) {
            $location .= " / city: $location_data->cityName";
        } else if ($location_data->regionName) {
            $location .= " / region: $location_data->regionName";
        }
        return $location;
    }
}
