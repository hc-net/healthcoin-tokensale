<?php

namespace App\Models\Accounts;

use App\Models\Auth\User;
use BaconQrCode\Renderer\Image\Png;
use BaconQrCode\Writer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * @property integer id
 * @property Currency currency
 * @property float balance
 * @property User user
 * @property string wallet
 * @property bool is_active
 * @property integer user_id
 * @property string status
 * @property float actual_balance
 * @property float pending_balance
 * @property float credit_limit
 */
class Account extends Model
{
    const STATUS_CREATED = 'created';
    const STATUS_OPEN = 'open';
    const STATUS_CLOSED = 'closed';

    const STATUSES = [
        self::STATUS_CREATED,
        self::STATUS_OPEN,
        self::STATUS_CLOSED,
    ];

    const TYPE_BANK_PAYMENT = 'bank_payment';
    const TYPE_USER_ETHER = 'user_ether';
    const TYPE_USER_ETHER_OLD = 'user_ether_old';
    const TYPE_COIN_SOURCE = 'coin_source';
    const TYPE_SELLER_COIN = 'seller_coin';
    const TYPE_TOKEN_SOURCE = 'token_source';
    const TYPE_USER_TOKEN = 'user_token';

    const TYPES = [
        self::TYPE_BANK_PAYMENT,
        self::TYPE_USER_ETHER,
        self::TYPE_USER_ETHER_OLD,
        self::TYPE_COIN_SOURCE,
        self::TYPE_SELLER_COIN,
        self::TYPE_TOKEN_SOURCE,
        self::TYPE_USER_TOKEN,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'status',
        'user_id',
        'currency_id',
        'balance',
        'credit_limit',
        'updated_at',
    ];

    ///////////////
    // Relations //
    ///////////////

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\Models\Auth\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency() {
        return $this->belongsTo('App\Models\Accounts\Currency');
    }

    public function transactions() {
        return $this->hasMany('App\Models\Accounts\Transaction');
    }

    ////////////////
    // Attributes //
    ////////////////

    public function getQrBase64Attribute()
    {
        if (!$this->wallet) {
            return '';
        }
        $renderer = new Png();
        $renderer->setHeight(136);
        $renderer->setWidth(136);
        $renderer->setMargin(0);
        $writer = new Writer($renderer);
        return 'data:image/png;base64,' . base64_encode($writer->writeString($this->wallet));
    }

    public function getActualBalanceAttribute()
    {
        return $this->transactions()->sum('amount');
    }

    /////////////
    // Methods //
    /////////////

    /**
     * @return bool
     */
    public function isOpen() {
        return $this->status === self::STATUS_OPEN;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canBeAuthorisedBy(User $user=null) {
        if ($user === null) {
            return true;
        }
        return !$this->user || $user->id === $this->user->id || $this->credit_limit === null;
    }

    /**
     * @param double $amount Sum to debit.
     * @return bool
     */
    public function isDebitPermitted($amount) {
        if ($this->credit_limit === null) {
            return true;
        }
        $available = bcadd($this->actual_balance, $this->credit_limit, 20);
        return bccomp($available, $amount, 20) >= 0;
    }

    public function getBalanceRoundAttribute()
    {
        $balance = floatval($this->balance);

        if ($balance == 0) {
            return 0;
        } else {
            if ($balance * 10 * 1000 >= 0.1) {
                return number_format($balance, 5);
            } else {
                return round($balance, 4);
            }
        }
    }

}
