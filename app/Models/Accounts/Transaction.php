<?php

namespace App\Models\Accounts;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'amount',
    ];

    public function transfer() {
        return $this->belongsTo('App\Models\Accounts\Transfer');
    }


    public function account() {
        return $this->belongsTo('App\Models\Accounts\Account');
    }

    public function scopeIncome($query)
    {
        return $query->where('transactions.amount', '>', 0);
    }
}
