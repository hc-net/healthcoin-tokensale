<?php

namespace App\Models\Auth;

use App\Models\Accounts\Account;
use App\Models\Accounts\AccountType;
use App\Models\Accounts\Currency;
use App\Models\Accounts\Transfer;
use App\Models\Accounts\WalletConfirmation;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property integer id
 * @property Account[] ether_accounts
 * @property Account ether_account
 * @property Account[] paymentsAccounts
 * @property string email
 */
class User extends Authenticatable
{

    const ROLE_ADMIN = 1;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_token', 'referral_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accounts()
    {
        return $this->hasMany('App\Models\Accounts\Account');
    }

    public function transfers()
    {
        return $this->hasMany(Transfer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function etherAccounts()
    {
        return $this->accounts()
            ->where('status', '!=', Account::STATUS_CLOSED)
            ->where('type', Account::TYPE_USER_ETHER)
            ->whereNotNull('wallet');
    }

    public function getEtherAccountAttribute()
    {
        return $this->etherAccounts()
            ->orderBy('created_at', 'desc')
            ->first();
    }

    /**
     * Кошельки пользователя с которых происходит оплата токенов. Только коины
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentsAccounts()
    {
        return $this->accounts()->where('type', Account::TYPE_BANK_PAYMENT);
    }

    /**
     * @param Currency $currency
     * @return Account|null
     */
    public function getBankAccount(Currency $currency)
    {
        return $this->paymentsAccounts()
            ->where('currency_id', $currency->id)
            ->first();
    }

    public function walletConfirmations() {
        $inLast24Houres = now()->subHour(24);
        return $this->hasMany(WalletConfirmation::class)
            ->where('created_at', '>', $inLast24Houres)
            ->whereNull('confirmed_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function referrals()
    {
        return $this->hasMany(self::class, 'referrer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referrer()
    {
        return $this->belongsTo(self::class, 'referrer_id');
    }

}
