<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Services\AccountService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('eth_address', function ($attribute, $value, $parameters, $validator) {
            if (env('DONT_VALIDATE_WALLET')) {
                return true;
            }
            return app(AccountService::class)->isValidETHAddress($value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
